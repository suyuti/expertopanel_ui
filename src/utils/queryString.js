export function makeQueryString(data) {
   return Object.keys(data).map(key => {
    let val = data[key]
    if (typeof val === 'object') val = makeQueryString(val)
    return `${key}=${encodeURIComponent(`${val}`.replace(/\s/g, '_'))}`
  }).join('&')
}