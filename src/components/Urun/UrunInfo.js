import React, { Fragment, useState, useEffect } from 'react';
import {
  Checkbox,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  TextField,
  InputAdornment,
  Box
} from '@material-ui/core';
import { useUrun } from '../../services/ApiService';
import NumberFormatCustom from '../../utils/NumberFormat';
import { useSelector } from 'react-redux';

const UrunFiyat = props => {
  const { label, fiyatVar, fiyat } = props;

  return (
    <div style={{ width: '100%' }}>
      <Box display="flex" p={1}>
        <Box p={1}>
          <Checkbox
            className="mb-24"
            checked={fiyatVar}
            //name="onOdemeTutariVar"
            //onChange={handleChange}
            //tabIndex={-1}
            disableRipple
            size="medium"
            //disabled={!editable}
          />
        </Box>
        <Box p={1} flexGrow={1}>
          <TextField
            className="mb-24"
            label={label}
            //id="onOdemeTutari"
            //name="onOdemeTutari"
            value={fiyat}
            //value={urun.onOdemeTutariVar ? urun.onOdemeTutari : ''}
            onChange={e => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                //      handleChange(e);
              }
            }}
            //type="text"
            variant="outlined"
            fullWidth
            //disabled={!urun.onOdemeTutariVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">TL</InputAdornment>
              )
              //readOnly: !editable,
            }}
          />
        </Box>
      </Box>
    </div>
  );
  /*
  return (
    <Grid container spacing={3} alignItems="flex-end">
      <Grid item>
        <Checkbox
          className="mb-24"
          checked={fiyatVar}
          //name="onOdemeTutariVar"
          //onChange={handleChange}
          //tabIndex={-1}
          disableRipple
          size="medium"
          //disabled={!editable}
        />
      </Grid>
      <Grid item>
        <TextField
          className="mb-24"
          label={label}
          //id="onOdemeTutari"
          //name="onOdemeTutari"
          value={fiyat}
          //value={urun.onOdemeTutariVar ? urun.onOdemeTutari : ''}
          onChange={e => {
            if (e.target.value.match(/^(\s*|\d+)$/)) {
              //      handleChange(e);
            }
          }}
          //type="text"
          variant="outlined"
          fullWidth
          //disabled={!urun.onOdemeTutariVar}
          InputProps={{
            startAdornment: <InputAdornment position="start">TL</InputAdornment>
            //readOnly: !editable,
          }}
        />
      </Grid>
    </Grid>
  );
  */
};

const UrunInfo = props => {
  const urunler = useSelector(state => state.ThemeOptions.urunler);
  const [urun, setUrun] = useState(null);

  useEffect(() => {
    const load = async () => {
      if (urunler) {
        const u = urunler.find(u => u._id == props.match.params.id);
        if (u) {
          setUrun(u);
        }
      }
    };
    load();
  }, []);
  if (!urun) {
    return <></>;
  }

  return (
    <Fragment>
      <Card>
        <CardHeader title="Urun Fiyatlari"></CardHeader>
        <Divider />
        <CardContent>
          <Grid container direction="row">
            <Grid item xs={12}>
              <TextField
                className="mb-4"
                variant="outlined"
                fullWidth
                label="Urun Adi"
                value={urun ? urun.adi : ''}
                onChange={e => {
                  var u = { ...urun };
                  u.adi = e.target.value;
                  setUrun(u);
                }}
              />
            </Grid>

            <Grid item xs={12}>
              <UrunFiyat
                label="On Odeme Tutari"
                fiyat={urun.onOdemeTutari}
                fiyatVar={urun.onOdemeTutariVar}
              />
            </Grid>
            <UrunFiyat
              label="Basari Tutari"
              fiyat={urun.basariTutari}
              fiyatVar={urun.basariTutariVar}
            />
            <UrunFiyat
              label="Sabit Tutar"
              fiyat={urun.sabitTutar}
              fiyatVar={urun.sabitTutarVar}
            />
            <UrunFiyat
              label="Rapor Tutari"
              fiyat={urun.raporBasiOdemeTutari}
              fiyatVar={urun.raporBasiOdemeTutariVar}
            />
            <UrunFiyat
              label="Yuzde Tutari"
              fiyat={urun.yuzdeTutari}
              fiyatVar={urun.yuzdeTutariVar}
            />
          </Grid>
        </CardContent>
        <CardActions></CardActions>
      </Card>
    </Fragment>
  );
};

export default UrunInfo;
