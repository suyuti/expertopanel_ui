import React, { Fragment, useState } from 'react';
import {
  Card,
  CardContent,
  Divider,
  Tooltip,
  IconButton,
  CardHeader,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Button,
  DialogActions,
  TextField,
  MenuItem
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Chart from 'react-apexcharts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import CountUp from 'react-countup';

const SatisPerformans = props => {
  const [openFormDialog, setOpenFormDialog] = useState(false);
  const chart3Options = {
    chart: {
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      stacked: true
    },
    dataLabels: {
      enabled: false
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '90%'
      }
    },
    colors: ['#4191ff', 'rgba(65, 145, 255, 0.35)'],
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    fill: {
      opacity: 1
    },
    legend: {
      show: false
    },
    labels: [
      'Ocak 2020',
      'Şubat 2020',
      'Mart 2020',
      'Nisan 2020',
      'Mayıs 2020',
      'Haziran 2020',
      'Temmuz 2020'
    ],
    xaxis: {
      crosshairs: {
        width: 1
      }
    },
    yaxis: {
      min: 0
    }
  };
  const chart3Data = [
    {
      name: 'Gerçekleşen',
      data: [2.3, 3.1, 4.0, 3.8, 5.1, 3.6, 4.0, 3.8, 5.1, 3.6, 3.2]
    },
    {
      name: 'Satış Hedefi',
      data: [2.1, 2.1, 3.0, 2.8, 4.0, 3.8, 5.1, 3.6, 4.1, 2.6, 1.2]
    }
  ];

  const handleClose = async => {
    setOpenFormDialog(false);
  };

  const aylar = [
    'Ocak',
    'Şubat',
    'Mart',
    'Nisan',
    'Mayıs',
    'Haziran',
    'Temmuz',
    'Ağustos',
    'Eylül',
    'Ekim',
    'Kasım',
    'Aralık'
  ];
  const yillar = [2020, 2021, 2022, 2023, 2024, 2025];

  return (
    <Fragment>
      <Card className="card-box mb-4">
        <CardHeader
          title="Satış Hedefleri"
          action={
            <Tooltip title="Yeni satış hedefi ver">
              <IconButton onClick={() => setOpenFormDialog(true)}>
                <AddIcon />
              </IconButton>
            </Tooltip>
          }
        />
        <Divider />
        <CardContent>
          <div className="px-4 pt-4 pb-3">
            <div className="text-uppercase text-muted">Satış Hedefi</div>
            <div className="d-flex align-items-center pt-3 font-weight-bold display-3">
              TL
              <span>
                <CountUp
                  start={0}
                  end={687}
                  duration={3}
                  deplay={2}
                  separator=""
                  decimals={0}
                  decimal=","
                />
              </span>
              <span className="text-danger pl-2 font-size-md">- 20%</span>
            </div>
          </div>
          <div className="p-1">
            <Chart
              options={chart3Options}
              series={chart3Data}
              type="bar"
              height={142}
            />
          </div>
        </CardContent>
      </Card>
      <Dialog
        open={openFormDialog}
        onClose={handleClose}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Satış Perfomans Hedefi</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Satış Perforamns hedefini aylık olarak belirleyin.
          </DialogContentText>
          <div className="p-4">
            <TextField
              className="m-3"
              label="Yıl"
              fullWidth
              variant="outlined"
              select>
              {yillar.map(y => (
                <MenuItem key={y} value={y}>
                  {y}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              className="m-3"
              label="Ay"
              fullWidth
              variant="outlined"
              select>
              {aylar.map(a => (
                <MenuItem key={a} value={a}>
                  {a}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <TextField
            className="m-3"
            //autoFocus
            //margin="dense"
            //id="name"
            label="Satış Hedefi"
            fullWidth
            variant="outlined"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            İptal
          </Button>
          <Button onClick={handleClose} color="primary">
            Tamam
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

export default SatisPerformans;
