import React from 'react';
import MaterialTable from 'material-table';
import { connect } from 'react-redux';
import useSWR from 'swr';
import axios from 'axios';
const BASE_URL = 'https://134.122.65.239:4000/v1'

const KisilerTable = props => {
  const token = window.localStorage.getItem('experto_access_token')
  const { data, error } = useSWR(`${BASE_URL}/kisi`, url =>
    axios.get(url, {headers: {authorization: token}}).then(resp => resp.data)
  );
  return (
    <MaterialTable
      title="Kişiler"
      //isLoading={!data}
      data={data}
      columns={[
        { title: 'Kontak Kişi', render: rowData => `${rowData.adi} ${rowData.soyadi}` }, 
        { title: 'Firma', field: 'musteri.marka'},
        { title: 'Ünvan', field: 'unvani'},
      ]}
      options={{
        pageSize: 100
      }}
      onRowClick={(e, rowData) => props.history.push(`/kisiler/${rowData._id}`)}
    />
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(KisilerTable);
