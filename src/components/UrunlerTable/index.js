import React from 'react';
import MaterialTable from 'material-table';
import { connect, useSelector } from 'react-redux';
import useSWR from 'swr';
import axios from 'axios';

const UrunlerTable = props => {
  const urunler = useSelector(state => state.ThemeOptions.urunler)

  return (
    <MaterialTable
      title="Ürünler"
      //isLoading={!data}
      data={urunler}
      columns={[{ title: 'Ürün Adı', field: 'adi' }]}
      options={{
        pageSize: 100
      }}
      onRowClick={(e, rowData) => props.history.push(`/urunler/${rowData._id}`)}
    />
  );
};

export default UrunlerTable