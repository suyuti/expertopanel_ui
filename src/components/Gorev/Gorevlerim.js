import React, { Fragment, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {getGorevlerim} from '../../actions/me.actions'

const Gorevlerim = props => {
    const dispatch = useDispatch()
    const gorevlerim = useSelector(state => state.ThemeOptions.gorevlerim)
    const me = useSelector(state => state.ThemeOptions.me)

    useEffect(() => {
        const load = async () => {
            if (me) {
                dispatch(getGorevlerim(me.id))
            }
        }
        load()
    }, [dispatch])

    return (
        <Fragment>
            {gorevlerim && gorevlerim.map(g => (<div>{g.title}</div>))}
        </Fragment>
    )
}

export default Gorevlerim