import React, { Fragment, useEffect, useState } from 'react';
import {
  List,
  Button,
  Grid,
  Card,
  Avatar,
  ListItem,
  CardContent,
  CardHeader,
  Divider,
  IconButton
} from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { connect, useDispatch, useSelector } from 'react-redux';
import { getMusteriKisiler } from 'actions/musteriler.actions';
import AddIcon  from '@material-ui/icons/Add';

const Kisi = props => {
  const { kisi } = props;
  return (
    <Fragment>
      <div className="d-flex align-items-center">
        <Avatar alt="..." className="mr-2" />
        <div>
          <a
            href="#/"
            onClick={e => e.preventDefault()}
            className="font-weight-bold text-black"
            title="...">
            kisi
          </a>
          <span className="text-black-50 d-block">unvan</span>
        </div>
      </div>
    </Fragment>
  );
};

const MusteriKisiler = props => {
  const dispatch = useDispatch();
  const kisiler = useSelector(state => state.ThemeOptions.musteriKisiler)
  const musteri = useSelector(state => state.ThemeOptions.musteri)

  //debugger
  useEffect(() => {
    const load = async () => {
      dispatch(getMusteriKisiler(musteri._id));
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Card className="card-box overflow-hidden mb-4">
        <CardHeader
          title="Firmadaki yetkili kisiler"
          action={<IconButton >
            <AddIcon />
          </IconButton>}
        />
        <Divider />
        <CardContent>
          <Grid container spacing={4}>
            {kisiler &&
              kisiler.map((k, i) => (
                <Grid item xs={3} key={i} className="m-4">
                  <Kisi kisi={k} />
                </Grid>
              ))}
          </Grid>
        </CardContent>
      </Card>
    </Fragment>
  );
};

export default MusteriKisiler
//const mapStateToProps = state => ({
//  kisiler: state.ThemeOptions.musteriKisiler
//});
//
//const mapDispatchToProps = dispatch => ({
//  //setSidebarToggleMobile: enable => dispatch(setSidebarToggleMobile(enable)),
//});
//
//export default connect(mapStateToProps, mapDispatchToProps)(MusteriKisiler);
