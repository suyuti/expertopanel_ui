import React, { Fragment, useState } from 'react';
import { Grid, TextField, Divider } from '@material-ui/core';
import GoogleMapReact from 'google-map-react';
import { useSelector, useDispatch } from 'react-redux';
import MusteriToolbar from './MusteriToolbar';
//import { SET_MUSTERI } from 'actions/musteriler.actions';
import { SET_MUSTERI } from 'reducers/ThemeOptions';

const MusteriIletisim = props => {
  const center = {
    lat: 41.009753,
    lng: 28.898915
  };
  const zoom = 11;

  const musteri = useSelector(state => state.ThemeOptions.musteri);
  const dispatch = useDispatch()

  const [ editMode, setEditMode ] = useState(false);

  return (
    <Fragment>
      <MusteriToolbar setEditMode={setEditMode} editMode={editMode} />
      <Divider />
      <Grid container spacing={3}>
        <Grid item xs={6} className='p-4'>
          <TextField
            className="m-2"
            fullWidth
            label="Adres"
            multiline
            rows={3}
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.iletisim.adres}
            onChange={e => {
              var m = { ...musteri };
              m.iletisim.adres = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
          <TextField
            className="m-2"
            fullWidth
            label="Fabrika Adresi"
            multiline
            rows={3}
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.iletisim.fabrikaAdresi}
            onChange={e => {
              var m = { ...musteri };
              m.iletisim.fabrikaAdresi = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
          <TextField
            className="m-2"
            fullWidth
            label="Fatura Adresi"
            multiline
            rows={3}
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.iletisim.faturaAdresi}
            onChange={e => {
              var m = { ...musteri };
              m.iletisim.faturaAdresi = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
          <TextField
            className="m-2"
            fullWidth
            label="OSB"
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.iletisim.osb}
            onChange={e => {
              var m = { ...musteri };
              m.iletisim.osb = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
          <TextField
            className="m-2"
            fullWidth
            label="Telefon"
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.iletisim.telefon}
            onChange={e => {
              var m = { ...musteri };
              m.iletisim.telefon = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
          <TextField
            className="m-2"
            fullWidth
            label="Web Sitesi"
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.iletisim.webSitesi}
            onChange={e => {
              var m = { ...musteri };
              m.iletisim.webSitesi = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
        </Grid>

        <Grid item xs={6} className='p-4'>
          <div className="w-100" style={{ height: '350px' }}>
            <GoogleMapReact
              defaultCenter={center}
              defaultZoom={zoom}></GoogleMapReact>
          </div>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriIletisim;
