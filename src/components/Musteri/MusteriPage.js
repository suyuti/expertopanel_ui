import React, { Fragment, useState, useEffect } from 'react';
import {
  ButtonGroup,
  Menu,
  MenuItem,
  FormControlLabel,
  Button,
  Grid,
  TextField,
  Divider,
  Typography,
  Avatar,
  IconButton,
  CircularProgress
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import IOSSwitch from 'components/IOSSwitch';
import AddIcon from '@material-ui/icons/Add';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useDispatch, useSelector } from 'react-redux';
import { getMusteri } from 'services/expertoService';
import MusteriToolbar from './MusteriToolbar';
import { SET_MUSTERI } from 'reducers/ThemeOptions';

const MusteriSection1 = props => {
  /* 
        Marka
        Unvan
        Sektor
        Kobi mi
    */

  const musteri = useSelector(state => state.ThemeOptions.musteri);
  const sektorler = useSelector(state => state.ThemeOptions.sektorler);
  const { editMode } = props;
  const dispatch = useDispatch();

  return (
    <Grid container>
      <Grid item xs={12} className="p-4">
        <Typography variant="h4">Firma Bilgi</Typography>
      </Grid>
      <Grid item xs={6} spacing={3} className="p-4">
        <TextField
          className="m-2"
          fullWidth
          label="Marka"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.marka}
          onChange={e => {
            var m = { ...musteri };
            m.marka = e.target.value;
            dispatch({ type: SET_MUSTERI, musteri: m });
          }}
        />
        <TextField
          className="m-2"
          fullWidth
          label="Firma Unvani"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.unvan}
          onChange={e => {
            var m = { ...musteri };
            m.unvan = e.target.value;
            dispatch({ type: SET_MUSTERI, musteri: m });
          }}
        />
      </Grid>
      <Grid item xs={6} spacing={3} className="p-4">
        {editMode ? (
          <Autocomplete
            id="sektorler"
            options={sektorler}
            getOptionLabel={option => option.adi}
            fullWidth
            autoHighlight={true}
            autoSelect={true}
            clearOnEscape={true}
            value={musteri.sektor && musteri.sektor._id}
            renderInput={params => (
              <TextField
                className="p-2"
                fullWidth
                {...params}
                label="Sektör"
                variant={editMode ? 'outlined' : 'standard'}
                //InputProps={{ readOnly: !editMode }}
              />
            )}
          />
        ) : (
          <TextField
            className="m-2"
            fullWidth
            label="Sektör"
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.sektor && musteri.sektor.adi}
          />
        )}
        <FormControlLabel
          control={
            <IOSSwitch
              checked={musteri.firmaBilgi.kobiMi}
              disabled={!editMode}
              onChange={e => {
                let checked = e.target.checked;
                var m = { ...musteri };
                m.firmaBilgi.kobiMi = checked;
                dispatch({ type: SET_MUSTERI, musteri: m });
              }}
              name="checkedB"
            />
          }
          label="Kobi mi?"
        />
      </Grid>
    </Grid>
  );
};

//-------------------------------------------------

const MusteriSection2 = props => {
  /* 
        Adres
        Fabrika adres
        Telefon
        Web
    */
  const { editMode, musteri, setMusteri } = props;
  return (
    <Grid container>
      <Grid item xs={12} className="p-4">
        <Typography variant="h4">Iletisim</Typography>
      </Grid>
      <Grid item xs={6} spacing={3} className="p-2">
        <TextField
          className="m-2"
          fullWidth
          label="Adres"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.iletisim.adres}
          onChange={e => {
            var m = { ...musteri };
            m.iletisim.adres = e.target.value;
            setMusteri(m);
          }}
        />
        <TextField
          className="m-2"
          fullWidth
          label="Fabrika Adresi"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.iletisim.fabrikaAdresi}
          onChange={e => {
            var m = { ...musteri };
            m.iletisim.fabrikaAdresi = e.target.value;
            setMusteri(m);
          }}
        />
        <TextField
          className="m-2"
          fullWidth
          label="Fabrika Adresi"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.iletisim.faturaAdresi}
          onChange={e => {
            var m = { ...musteri };
            m.iletisim.faturaAdresi = e.target.value;
            setMusteri(m);
          }}
        />
        <TextField
          className="m-2"
          fullWidth
          label="OSB"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.iletisim.osb}
          onChange={e => {
            var m = { ...musteri };
            m.iletisim.osb = e.target.value;
            setMusteri(m);
          }}
        />
      </Grid>
      <Grid item xs={6} spacing={3} className="p-2">
        <TextField
          className="m-2"
          fullWidth
          label="Telefon"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.iletisim.telefon}
          onChange={e => {
            var m = { ...musteri };
            m.iletisim.telefon = e.target.value;
            setMusteri(m);
          }}
        />
        <TextField
          className="m-2"
          fullWidth
          label="Web Sitesi"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.iletisim.webSitesi}
          onChange={e => {
            var m = { ...musteri };
            m.iletisim.webSitesi = e.target.value;
            setMusteri(m);
          }}
        />
      </Grid>
    </Grid>
  );
};

//-------------------------------------------------

const MusteriSection3 = props => {
  /* 
        Yillik ciro
    */
  const { editMode, musteri, setMusteri } = props;
  return (
    <Grid container>
      <Grid item xs={12} className="p-4">
        <Typography variant="h4">Mali Bilgiler</Typography>
      </Grid>
      <Grid item xs={3} spacing={3} className="p-2">
        <TextField
          className="m-2"
          fullWidth
          label="Vergi Dairesi"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.firmaBilgi.vergiDairesi}
          onChange={e => {
            var m = { ...musteri };
            m.firmaBilgi.vergiDairesi = e.target.value;
            setMusteri(m);
          }}
        />
        <TextField
          className="m-2"
          fullWidth
          label="Vergi Numarasi"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.firmaBilgi.vergiNo}
          onChange={e => {
            var m = { ...musteri };
            m.firmaBilgi.vergiNo = e.target.value;
            setMusteri(m);
          }}
        />
      </Grid>
      <Grid item xs={3} spacing={3} className="p-2">
        <TextField
          className="m-2"
          fullWidth
          label="Yillik Cirosu"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
          value={musteri.firmaBilgi.ciro}
          onChange={e => {
            var m = { ...musteri };
            m.firmaBilgi.ciro = e.target.value;
            setMusteri(m);
          }}
        />
        <FormControlLabel
          control={
            <IOSSwitch
              checked={musteri.firmaBilgi.eFaturaMi}
              disabled={!editMode}
              onChange={e => {
                let checked = e.target.checked;
                var m = { ...musteri };
                m.firmaBilgi.eFaturaMi = checked;
                setMusteri(m);
              }}
              name="checkedB"
            />
          }
          label="E-Fatura mı?"
        />
      </Grid>
    </Grid>
  );
};

//-------------------------------------------------

const MusteriSection4 = props => {
  /* 
        Kisiler
    */
  return (
    <Grid container>
      <Grid item xs={12} className="p-4">
        <Typography variant="h4">Ilgili Kisiler</Typography>
        <Button>Ekle</Button>
      </Grid>
      <Grid item xs={12} spacing={3} className="p-2">
        <span>
          <Avatar />
        </span>
        <span>
          <Avatar />
        </span>
        <span>
          <Avatar />
        </span>
        <span>
          <Avatar />
        </span>
      </Grid>
    </Grid>
  );
};

//-------------------------------------------------

const MusteriSection5 = props => {
  /* 
          Calısan bılgıleri
      */
  const { editMode } = props;
  return (
    <Grid container>
      <Grid item xs={12} className="p-4">
        <Typography variant="h4">Çalışan Kadrosu</Typography>
      </Grid>
      <Grid item xs={6} spacing={3} className="p-2">
        <TextField
          className="m-2"
          fullWidth
          label="Toplam Personel"
          variant={editMode ? 'outlined' : 'standard'}
          InputProps={{ readOnly: !editMode }}
        />
      </Grid>
    </Grid>
  );
};

//-------------------------------------------------

const MusteriPage = props => {
  const [editMode, setEditMode] = useState(false);
  const musteri = useSelector(state => state.ThemeOptions.musteri);
  //const [musteri_, setMusteri] = useState(null);

  if (!musteri) {
    return <CircularProgress />;
  }
  return (
    <Fragment>
      <MusteriToolbar setEditMode={setEditMode} editMode={editMode} />
      <Divider />
      <MusteriSection1
        editMode={editMode}
        //musteri={musteri}
        //setMusteri={setMusteri}
      />
    </Fragment>
  );
};

export default MusteriPage;
