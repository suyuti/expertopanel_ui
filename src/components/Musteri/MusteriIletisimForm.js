import React, {useState} from 'react'
import {Grid, Card, Divider, TextField} from '@material-ui/core'

import MaskedInput from 'react-text-mask';
function TextMaskCustom(props) {
    const { inputRef, ...other } = props;
  
    return (
      <MaskedInput
        {...other}
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={[
          '(',
          /[1-9]/,
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ]}
        placeholderChar={'\u2000'}
        showMask
      />
    );
  }
  

const MusteriIletisimForm = props => {
    const {musteri} = props
    const [form, setForm] = useState(musteri)

    return (
        <Card className='p-4 mb-4'>
            <div className='font-size-lg font-weight-bold'>Iletisim Bilgileri</div>
            <Divider className='my-4' />
            <Grid container spacing={4}>
                <Grid item xs={12} lg={12}>
                    <div className='p-1'>
                    <TextField 
                            fullWidth
                            className='m-2'
                            label='Adres'
                            value={form.iletisim.adres}
                            onChange={(e) => {
                                var f = {...form}
                                f.iletisim.adres = e.target.value
                                setForm(f)
                            }}
                        />
                        <TextField 
                            fullWidth
                            className='m-2'
                            label='Fatura Adresi'
                            value={form.iletisim.faturaAdresi}
                            onChange={(e) => {
                                var f = {...form}
                                f.iletisim.faturaAdresi = e.target.value
                                setForm(f)
                            }}
                        />
                        <TextField 
                            fullWidth
                            className='m-2'
                            label='Telefon'
                            InputProps={{
                                inputComponent: TextMaskCustom
                              }}
                        />
                        <TextField 
                            fullWidth
                            className='m-2'
                            label='Web Sitesi'
                        />
                    </div>
                </Grid>
            </Grid>
        </Card>    )
}

export default MusteriIletisimForm