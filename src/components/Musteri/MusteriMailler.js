import React, { useState, useEffect } from 'react';
import MaterialTable from 'material-table';
import { getMails } from '../../services/GraphService';
import moment from 'moment';
import { Typography } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {useEmails} from '../../services/GraphService'

const MusteriMailler = props => {
  const token = window.localStorage.getItem('token');
  const [mails, setMails] = useState([]);
  
  useEmails(token, '@gmail.com').then(m=> setMails(m))
/*
  useEffect(() => {
    getMails(token, '@experto.com').then(e => {
      setMails(e.value.sort((a,b)=> a.sentDateTime<b.sentDateTime?1:(a.sentDateTime>b.sentDateTime)?-1:0));
    });
  }, []);*/

  if (!mails) {
    return <></>;
  }

  return (
    <MaterialTable
      title="Mailler"
      //isLoading={!data}
      detailPanel={rowData => {
        return (
          <div className='p-3' dangerouslySetInnerHTML={{ __html: rowData.body.content }}></div>
        );
      }}
      data={mails}
      columns={[
        { title: 'Konu', field: 'subject' },
        { title: 'Gönderen', field: 'from.emailAddress.name' },
        {
          title: 'Tarih',
          render: rowData =>
            moment(rowData.sentDateTime).format('HH:mm DD.MM.YYYY')
        },
        {title: 'Ek', render: rowData => rowData.hasAttachments?<FontAwesomeIcon size='2x' icon={['fa', 'paperclip']} />:''}
      ]}
      options={{
        pageSize: 100,
        padding:'dense'
      }}
      //onRowClick={(e, rowData) => props.history.push(`/musteriler/${rowData._id}`)}
    />
  );
};

export default MusteriMailler;
