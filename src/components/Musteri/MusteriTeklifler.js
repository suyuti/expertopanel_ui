import React, { Fragment, useState } from 'react';
import { Grid, Typography, Divider, Card, CardHeader, CardContent, IconButton, Table, TableHead, TableRow, TableCell } from '@material-ui/core';
import { useSelector } from 'react-redux';
import AddIcon from '@material-ui/icons/Add'

const MusteriTeklifler = props => {
  const musteri = useSelector(state => state.ThemeOptions.musteri);
  const [editMode, setEditMode] = useState(false)
  
  return (
    <Fragment>
      <Grid container spacing={3}>
          <Grid item xs={12}>
              <Card>
                <CardHeader 
                  title='Teklifler'
                  subheader = 'Müşteriye verilmiş teklifler'
                  action={<IconButton>
                    <AddIcon />
                  </IconButton>}
                />
                <Divider />
                <CardContent>
                  <Table>
                    <TableHead>
                      <TableRow>
                      <TableCell>Teklif</TableCell>
                      <TableCell>Ürün</TableCell>
                      <TableCell>Fiyat</TableCell>
                      <TableCell>Tarih</TableCell>
                      <TableCell>Durum</TableCell>
                      </TableRow>
                    </TableHead>
                  </Table>
                </CardContent>
              </Card>
          </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriTeklifler;
