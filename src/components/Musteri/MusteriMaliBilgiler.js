import React, { Fragment, useState } from 'react';
import { Grid, TextField, Divider } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import MusteriToolbar from './MusteriToolbar';
import { SET_MUSTERI } from 'reducers/ThemeOptions';

const MusteriMaliBilgiler = props => {
  const dispatch = useDispatch()
  const musteri = useSelector(state => state.ThemeOptions.musteri);
  const [editMode, setEditMode] = useState(false)
  
  return (
    <Fragment>
      <MusteriToolbar setEditMode={setEditMode} editMode={editMode} />
      <Divider />
      <Grid container spacing={3}>
        <Grid item xs={6} className="p-4">
          <TextField
            className="m-2"
            fullWidth
            label="Ciro"
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            value={musteri.firmaBilgi.ciro}
            onChange={e => {
              var m = { ...musteri };
              m.firmaBilgi.ciro = e.target.value;
              dispatch({type: SET_MUSTERI, musteri: m})
            }}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriMaliBilgiler;
