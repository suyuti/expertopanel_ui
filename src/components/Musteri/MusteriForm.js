import React, {useState} from 'react'
import { Grid, Card, Divider, TextField, MenuItem } from '@material-ui/core'
import { useSektorler } from 'services/ApiService'
import Autocomplete from '@material-ui/lab/Autocomplete'

const MusteriForm = props => {
    const {musteri} = props
    const {sektorler, isLoading, isError} = useSektorler()
    const [form, setForm] = useState(musteri)

    return (
        <Card className='p-4 mb-4'>
            <div className='font-size-lg font-weight-bold'>Musteri</div>
            <Divider className='my-4' />
            <Grid container spacing={4}>
                <Grid item xs={12} lg={12}>
                    <div className='p-1'>
                    <TextField 
                            fullWidth
                            className='m-2'
                            label='Firma Markası'
                            value={form.marka}
                            onChange={(e) => {
                                var f = {...form}
                                f.marka = e.target.value
                                setForm(f)
                            }}
                        />
                        <TextField 
                            fullWidth
                            className='m-2'
                            label='Firma Ünvanı'
                            value={form.unvani}
                            onChange={(e) => {
                                var f = {...form}
                                f.unvani = e.target.value
                                setForm(f)
                            }}
                        />
                        <Autocomplete 
                            id='sektorler'
                            options={sektorler}
                            getOptionLabel={(option) => option.adi}
                            fullWidth
                            autoHighlight={true}
                            autoSelect={true}
                            clearOnEscape={true}
                            renderInput={(params) => <TextField className='m-2' fullWidth {...params} label="Sektör"  />}
                        />
                    </div>
                </Grid>
            </Grid>
        </Card>
    )
}
export default MusteriForm