import React from "react"
import {Menu, MenuItem, Grid, Button, ButtonGroup} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {saveMusteri} from 'actions/musteriler.actions'
import { useSelector, useDispatch } from "react-redux";

const MusteriToolbar = props => {
    const { editMode, setEditMode } = props;
    const [anchorEl, setAnchorEl] = React.useState(null);
    const musteri = useSelector(state => state.ThemeOptions.musteri)
    const dispatch = useDispatch()
  
    const handleClick = event => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    const save = () => {
      dispatch(saveMusteri(musteri))
    }
  
    return (
      <Grid container>
        <Button
          //variant="outlined"
          color="primary"
          className="m-2"
          onClick={() => {
            if (editMode) {
              save()
              setEditMode(!editMode)
            }  
            else {
              setEditMode(!editMode)
            }
          }}>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['far', 'lightbulb']} />
          </span>
          <span className="btn-wrapper--label">
            {editMode ? 'Kaydet' : 'Değiştir'}
          </span>
        </Button>
  
        <Button 
          //variant="outlined" 
          color="primary" className="m-2">
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['far', 'lightbulb']} />
          </span>
          <span className="btn-wrapper--label">Refresh</span>
        </Button>
  
        <Button 
          //variant="outlined" 
          color="primary" className="m-2">
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['far', 'lightbulb']} />
          </span>
          <span className="btn-wrapper--label">Sil</span>
        </Button>
  
        <ButtonGroup
          className="m-2"
          //variant="outlined"
          color="primary"
          size="small"
          aria-label="split button">
          <Button size="small">Durum Değiştir</Button>
          <Button
            color="primary"
            size="small"
            aria-haspopup="true"
            onClick={handleClick}>
            <ArrowDropDownIcon />
          </Button>
        </ButtonGroup>
        <Menu
          id="simple-menu2"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}>
          <MenuItem onClick={handleClose}>Aktif</MenuItem>
          <MenuItem onClick={handleClose}>Potansiyel</MenuItem>
          <MenuItem onClick={handleClose}>Pasif</MenuItem>
          <MenuItem onClick={handleClose}>Sorunlu</MenuItem>
        </Menu>
  
      </Grid>
    );
  };
  
  export default MusteriToolbar