import React, { Fragment, useState } from 'react';
import { Grid, Typography, Divider, Card, CardHeader, CardContent, IconButton, Table, TableHead, TableRow, TableCell } from '@material-ui/core';
import { useSelector } from 'react-redux';
import AddIcon from '@material-ui/icons/Add'

const MusteriSozlesmeler = props => {
  const musteri = useSelector(state => state.ThemeOptions.musteri);
  const [editMode, setEditMode] = useState(false)
  
  return (
    <Fragment>
      <Grid container spacing={3}>
          <Grid item xs={12}>
              <Card>
                <CardHeader 
                  title='Sözleşmeler'
                  subheader = 'Müşteri sözleşmeleri'
                  action={<IconButton disabled>
                    <AddIcon />
                  </IconButton>}
                />
                <Divider />
                <CardContent>
                  <Table>
                    <TableHead>
                      <TableRow>
                      <TableCell>Sözleşme No</TableCell>
                      <TableCell>Ürün</TableCell>
                      <TableCell>Başlangıç Tarihi</TableCell>
                      <TableCell>Bitiş Tarihi</TableCell>
                      <TableCell>Durum</TableCell>
                      </TableRow>
                    </TableHead>
                  </Table>
                </CardContent>
              </Card>
          </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriSozlesmeler;
