import React, { useEffect, useState, Fragment } from 'react';
import MaterialTable from 'material-table';
import { connect, useSelector } from 'react-redux';
import useSWR from 'swr';
import axios from 'axios';
import { useDispatch } from 'react-redux';
//import * as Actions from '../../../actions/musteriler.actions';
import { useMusteriler } from 'services/ApiService';
import {
  Avatar,
  Menu,
  MenuItem,
  List,
  ListItem,
  Divider,
  Button,
  Typography
} from '@material-ui/core';
import { getUser } from '../../../services/GraphService';
import MusteriTemsilcisi from '../MusteriTemsilcisi';
import { useGroup } from '../../../services/GraphService';
import { CircularProgress } from '@material-ui/core';
//import { getMusteriler } from 'services/expertoService';
import { getGroupByName } from 'services/graph_Service';
import { getUsers } from 'actions/me.actions';
import { getMusteriler } from 'actions/musteriler.actions';

var qs = require('qs');
const queryString = require('query-string');

const Temsilci = props => {
  const { token, userId } = props;
  const [user, setUser] = useState(null);

  useEffect(() => {
    getUser(token, userId).then(user => {
      setUser(user);
    });
  }, []);

  if (!user) {
    return <></>;
  }

  return (
    <Fragment>
      <div className="d-flex align-items-center">
        <Avatar alt="..." src={user.photo} className="mr-2" />
        <div>
          <a
            href="#/"
            onClick={e => e.preventDefault()}
            className="font-weight-bold text-black"
            title="...">
            {user.displayName}
          </a>
          <span className="text-black-50 d-block">{user.jobTitle}</span>
        </div>
      </div>
    </Fragment>
  );
};

const MusterilerTable = props => {
  //const [musteriler, setMusteriler] = useState([])
  //const dispatch = useDispatch()
  const token = window.localStorage.getItem('token');
  const params = queryString.parse(props.location.search);
  //const { musteriler, isLoading, isError }  = useMusteriler({ filter: params });
  //const [group, setGroup] = useState(null)
  //const [musteriler, setMusteriler] = useState(null)
  const [group, setGroupMembers] = useState(null);
  const dispatch = useDispatch();
  const users = useSelector(state => state.ThemeOptions.users);
  const tumMusteriler = useSelector(state => state.ThemeOptions.musteriler);
  const [musteriler, setMusteriler] = useState(null);
  const [anchorElMenu, setAnchorElMenu] = useState(null);

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorElMenu(null);
  };

  const handleTemsilci = id => {
    //APISetMusteriTemsilcisi(musteri._id, id)
    handleCloseMenu();
  };

  useEffect(() => {
    const load = async () => {
      if (!tumMusteriler) {
        dispatch(getMusteriler());
      } else {
        setMusteriler(tumMusteriler.filter(m => m.durum === params.durum));
      }
      if (!users) {
        dispatch(getUsers());
      }
    };
    load();
  }, [props.location.search, dispatch, tumMusteriler]);

  if (!musteriler || !users) {
    return (
      <>
        <CircularProgress color="secondary" />
      </>
    );
  }
  const menu = (
    <Menu
      anchorEl={anchorElMenu}
      keepMounted
      open={Boolean(anchorElMenu)}
      onClose={handleCloseMenu}
      getContentAnchorEl={null}
      classes={{ list: 'p-0' }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}>
      <div className="overflow-hidden dropdown-menu-xl p-0">
        <div className="bg-composed-wrapper bg-light mt-0">
          <div className="bg-composed-wrapper--content text-black px-4 py-3">
            <h5 className="mb-1">Atanacak Personeller</h5>
            <p className="mb-0 opacity-7"></p>
          </div>
        </div>
        <List>
          {users.map(p => {
            return (
              <Fragment>
                <ListItem
                  button
                  className="align-box-row"
                  onClick={() => handleTemsilci(p.id)}>
                  <div className="d-flex align-items-center">
                    <Avatar className="mr-4" src={p.photo} />
                    {p.displayName}
                    <span className="text-black">{p.jobTitle}</span>
                  </div>
                </ListItem>
                <Divider />
              </Fragment>
            );
          })}
        </List>
      </div>
    </Menu>
  );

  return (
    <Fragment>
      <MaterialTable
        title="Müşteriler"
        //isLoading={isLoading}
        data={musteriler}
        columns={[
          { title: 'Marka', field: 'marka' },
          {
            title: 'Müşteri Temsilcisi',
            render: rowData => {
              let user = users.find(u => u.id == rowData.musteriTemsilcisi);
              if (user) {
                return (
                  <Fragment>
                    <div className="d-flex align-items-center">
                      <Avatar alt="..." src={user.photo} className="mr-2" />
                      <div>
                        <a
                          href="#/"
                          onClick={e => e.preventDefault()}
                          className="font-weight-bold text-black"
                          title="...">
                          {user.displayName}
                        </a>
                        <span className="text-black-50 d-block">
                          {user.jobTitle}
                        </span>
                      </div>
                    </div>
                  </Fragment>
                );
              } else {
                return (
                  <Fragment>
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={handleClickMenu}>
                      Musteri Temsilcisi Ata
                    </Button>
                  </Fragment>
                );
              }
            }
          }
        ]}
        actions={[
          {
            icon: 'edit',
            tooltip: 'Detail',
            onClick: (event, rowData) =>
              props.history.push(`/musteriler/${rowData._id}`),
            disabled: false // TODO Yetkisi yoksa goremeyecek
          }
        ]}
        options={{
          actionsColumnIndex: -1,
          pageSize: 20
        }}
      />
      {menu}
    </Fragment>
  );
};

export default MusterilerTable;
