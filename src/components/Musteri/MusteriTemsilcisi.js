import React, { Fragment, useState, useEffect } from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';
import {
  Grid,
  Avatar,
  Card,
  Button,
  Menu,
  List,
  ListItem,
  Divider,
  CircularProgress
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  getAllPersonsOnMyGroups,
  getGroupByName,
  getGroupMembersByName,
  getUser,
  getUserList,
  _getUserPhoto
} from '../../services/GraphService';

import { setMusteriTemsilcisi as APISetMusteriTemsilcisi } from '../../services/ApiService';
import useSWR from 'swr';
import axios from 'axios';
import { getUserPhoto } from 'services/graph_Service';
import { getUsers } from 'actions/me.actions';

const PersonelItem = props => {
  const { user } = props;
  const [photo, setPhoto] = useState(null);
  const [photoVar, setPhotoVar] = useState(0);

  /*  useEffect(() => {
    const load = async () => {
      if (user) {
        getUserPhoto(user.id).then(photo => {
          console.log(photo);
          if (photo) {
            setPhoto(photo);
            setPhotoVar(1);
          } else {
            setPhotoVar(-1);
          }
        });
      }
    };
    load();
  }, []);

  var avatar = <></>;
  if (photoVar == 1) {
    avatar = <Avatar className="mr-4" src={photo} />;
  } else if (avatar == -1) {
    console.log('BOS avatar');
    avatar = <Avatar className="mr-4" />;
  } else {
    avatar = <Avatar className="mr-4" />;
  }
*/
  return (
    <Fragment>
      <div className="d-flex align-items-center">
        <Avatar className="mr-4" src={user.photo} />
        {user.displayName}
        <span className="text-black">{user.jobTitle}</span>
      </div>
    </Fragment>
  );
};

const MusteriTemsilcisi = props => {
  const { musteriTemsilcisi, group } = props;
  const users = useSelector(state => state.ThemeOptions.users);
  const [user, setUser] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    const load = async () => {
      if (!users) {
        dispatch(getUsers())
        return;
      }
      if (musteriTemsilcisi) {
        const u = users.find(u => u.id === musteriTemsilcisi);
        if (u) {
          setUser(u);
        }
      }
    };
    //load();
  }, [dispatch, users]);

  const [anchorElMenu, setAnchorElMenu] = useState(null);
  //  const token = window.localStorage.getItem('token');
  //  const [personeller, setPersoneller] = useState(group);
  //  const [musteriTemsilcisi, setMusteriTemsilcisi] = useState(null);
  //  const [photo, setPhoto] = useState(null)

  //  useEffect(() => {
  //    const load = async () => {
  //      if (musteri.musteriTemsilcisi) {
  //        getUserPhoto(musteri.musteriTemsilcisi).then(photo => setPhoto(photo))
  //      }
  //    }
  //    load()
  //  }, [])

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorElMenu(null);
  };

  const handleTemsilci = id => {
    //APISetMusteriTemsilcisi(musteri._id, id)
    handleCloseMenu();
  };


  if (user) {
    return (
      <Fragment>
        <div className="d-flex align-items-center">
          <Avatar alt="..." src={user.photo} className="mr-2" />
          <div>
            <a
              href="#/"
              onClick={e => e.preventDefault()}
              className="font-weight-bold text-black"
              title="...">
              {user.displayName}
            </a>
            <span className="text-black-50 d-block">{user.jobTitle}</span>
          </div>
        </div>
      </Fragment>
    );
  } else {
    return (
      <Fragment>
        <Button variant="outlined" color="primary" onClick={handleClickMenu}>
          Musteri Temsilcisi Ata
        </Button>
        <Menu
          anchorEl={anchorElMenu}
          keepMounted
          open={Boolean(anchorElMenu)}
          onClose={handleCloseMenu}
          getContentAnchorEl={null}
          classes={{ list: 'p-0' }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}>
          <div className="overflow-hidden dropdown-menu-xl p-0">
            <div className="bg-composed-wrapper bg-light mt-0">
              <div className="bg-composed-wrapper--content text-black px-4 py-3">
                <h5 className="mb-1">Atanacak Personeller</h5>
                <p className="mb-0 opacity-7"></p>
              </div>
            </div>
            <List>
              {users && users.map(p => {
                return (
                  <Fragment>
                    <ListItem
                      button
                      className="align-box-row"
                      onClick={() => handleTemsilci(p.id)}>
                      <PersonelItem user={p} />
                    </ListItem>
                    <Divider />
                  </Fragment>
                );
              })}
            </List>
          </div>
        </Menu>
      </Fragment>
    );
  }
  /*
  if (musteri && musteri.musteriTemsilcisi) {
    const t = group.find(m => m.id == musteri.musteriTemsilcisi)
    return (
      <Fragment>
      <div className="d-flex align-items-center">
        <Avatar alt="..." src={photo} className="mr-2" />
        <div>
          <a
            href="#/"
            onClick={e => e.preventDefault()}
            className="font-weight-bold text-black"
            title="...">
            {t ? t.displayName : 'yok'}
          </a>
          <span className="text-black-50 d-block">
            {t && t.jobTitle}
          </span>
        </div>
      </div>
    </Fragment>
    );
  } else {
    return (
      <Fragment>
        <Button variant="outlined" color="primary" onClick={handleClickMenu}>
          Musteri Temsilcisi Ata
        </Button>
        <Menu
          anchorEl={anchorElMenu}
          keepMounted
          open={Boolean(anchorElMenu)}
          onClose={handleCloseMenu}
          getContentAnchorEl={null}
          classes={{ list: 'p-0' }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}>
          <div className="overflow-hidden dropdown-menu-xl p-0">
            <div className="bg-composed-wrapper bg-light mt-0">
              <div className="bg-composed-wrapper--content text-black px-4 py-3">
                <h5 className="mb-1">Atanacak Personeller</h5>
                <p className="mb-0 opacity-7"></p>
              </div>
            </div>
            <List>
              {personeller.map(p => {
                return (
                  <Fragment>
                    <ListItem 
                      button 
                      className="align-box-row"
                      onClick={() => handleTemsilci(p.id)}
                      >
                        <PersonelItem token={token} user={p} />
                    </ListItem>
                    <Divider />
                  </Fragment>
                );
              })}
            </List>
          </div>
        </Menu>
      </Fragment>
    );
  }
  */
};

//const mapStateToProps = state => ({
//  personeller: state.ThemeOptions.personeller,
//});

//const mapDispatchToProps = dispatch => ({
//  setPersoneller: dispatch(setPersoneller(personeller))
//});

//export default connect(mapStateToProps, mapDispatchToProps)(MusteriTemsilcisi);
export default MusteriTemsilcisi;
