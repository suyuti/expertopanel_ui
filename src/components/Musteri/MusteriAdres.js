import React, { Fragment, useState } from 'react';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  Divider,
  TextField,
  Button
} from '@material-ui/core';
import { updateMusteri } from '../../services/expertoService';

const MusteriAdres = props => {
  const { musteri } = props;
  const [editMode, setEditMode] = useState(false);
  const [form, setForm] = useState({ iletisim: {} });

  const save = async () => {
    updateMusteri(musteri._id, form).then(resp => {
      setEditMode(false);
    });
  };

  return (
    <Fragment>
      <Card key="" className="mb-4">
        <CardContent>
          <Grid container spacing={4}>
            <Grid item continer xs={12}>
              <Grid item xs={10}>
                <Typography>Adres</Typography>
              </Grid>
              <Grid item xs={2}>
                <Button
                  variant="outlined"
                  size="small"
                  onClick={() => {
                    if (editMode) {
                      save();
                    } else {
                      setEditMode(true);
                    }
                  }}>
                  {editMode ? 'Save' : 'Edit'}
                </Button>
              </Grid>
            </Grid>
            <Divider />
            <Grid item xs={12}>
              <TextField
                className="mb-4"
                label="Firma Adresi"
                value={form.iletisim.adres}
                fullWidth
                InputProps={{ readOnly: !editMode }}
                onChange={e => {
                  var f = { ...form };
                  f.iletisim.adres = e.target.value;
                  setForm(f);
                }}
              />
              <TextField
                className="mb-4"
                label="Fatura Adresi"
                value={form.faturaAdresi}
                fullWidth
                InputProps={{ readOnly: !editMode }}
                onChange={e => {
                  var f = { ...form };
                  f.iletisim.faturaAdresi = e.target.value;
                  setForm(f);
                }}
              />
              <TextField
                className="mb-4"
                label="Fabrika Adresi"
                value={form.fabrikaAdresi}
                fullWidth
                InputProps={{ readOnly: !editMode }}
                onChange={e => {
                  var f = { ...form };
                  f.iletisim.fabrikaAdresi = e.target.value;
                  setForm(f);
                }}
              />
              <TextField
                className="mb-4"
                label="Telefon"
                value={form.telefon}
                fullWidth
                InputProps={{ readOnly: !editMode }}
                onChange={e => {
                  var f = { ...form };
                  f.iletisim.telefon = e.target.value;
                  setForm(f);
                }}
              />
              <TextField
                className="mb-4"
                label="Web Sistesi"
                value={form.webSitesi}
                fullWidth
                InputProps={{ readOnly: !editMode }}
                onChange={e => {
                  var f = { ...form };
                  f.iletisim.webSitesi = e.target.value;
                  setForm(f);
                }}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Fragment>
  );
};

export default MusteriAdres;
