import React, { Fragment } from 'react';
import {
  Button,
  Grid,
  Tooltip,
  Card,
  CardContent,
  Fab,
  IconButton,
  Divider
} from '@material-ui/core';
import AddCircleTwoToneIcon from '@material-ui/icons/AddCircleTwoTone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import avatar1 from '../../../assets/images/avatars/avatar1.jpg';
import avatar2 from '../../../assets/images/avatars/avatar2.jpg';
import avatar3 from '../../../assets/images/avatars/avatar3.jpg';
import avatar4 from '../../../assets/images/avatars/avatar4.jpg';
import avatar6 from '../../../assets/images/avatars/avatar6.jpg';
import avatar7 from '../../../assets/images/avatars/avatar7.jpg';

import { Settings, Briefcase, Users, Layers } from 'react-feather';

import MusteriKisilerTable from './MusteriKisilerTable'
import MusteriTemsilcisi from '../MusteriTemsilcisi';

const MusteriInfo = props => {
  const { musteri } = props;
  if (!musteri) {
    return <></>;
  }

  return (
    <Fragment>
      <Card key={props.musteri} className="mb-4">
        <div className="card-header pr-2 text-right">
          <div className="card-header--actions">
            <Button
              size="small"
              variant="outlined"
              color="primary"
              className="mr-3"
              onClick={() => {
                props.history.push(`/musteriler/${musteri._id}/edit`);
              }}>
              Güncelle
            </Button>
          </div>
        </div>
          <div className="bg-composed-wrapper--content text-center  p-5">
            <h1 className="display-4 pt-3 pb-1 font-weight-bold mb-0">
              {musteri.marka}
            </h1>
            <p className="text-black-50 mb-3">
              {musteri.sektor ? musteri.sektor.adi : 'Sektor tanımlı değil'}
            </p>
        </div>
        <Divider />
        <div className="card-body text-center">
          <Grid container spacing={4}>
            <Grid xs={12} sm={4} item>
              <div className="bg-secondary p-3 text-center rounded">
                <div>
                  <FontAwesomeIcon
                    icon={['far', 'user']}
                    className="font-size-xxl text-warning"
                  />
                </div>
                <div className="mt-2 line-height-sm">
                  <b className="font-size-lg">2,345</b>
                  <span className="text-black-50 d-block">Personel</span>
                </div>
              </div>
            </Grid>
            <Grid xs={12} sm={4} item>
              <div className="bg-secondary p-3 text-center rounded">
                <div>
                  <FontAwesomeIcon
                    icon={['fas', 'lemon']}
                    className="font-size-xxl text-success"
                  />
                </div>
                <div className="mt-2 line-height-sm">
                  <b className="font-size-lg">$3,586</b>
                  <span className="text-black-50 d-block">Proje</span>
                </div>
              </div>
            </Grid>
            <Grid xs={12} sm={4} item>
              <div className="bg-secondary p-3 text-center rounded">
                <div>
                  <FontAwesomeIcon
                    icon={['far', 'chart-bar']}
                    className="font-size-xxl text-info"
                  />
                </div>
                <div className="mt-2 line-height-sm">
                  <b className="font-size-lg">$9,693</b>
                  <span className="text-black-50 d-block">revenue</span>
                </div>
              </div>
            </Grid>
          </Grid>
          
          <MusteriKisilerTable musteri={musteri}/>




        </div>

        <Divider />

        <div className="d-flex p-4 bg-secondary card-footer mt-4 flex-wrap">
          <div className="w-50 p-2">
            <button className="btn card card-box text-left d-flex justify-content-center px-4 py-3 w-100">
              <div>
                <Settings className="h1 d-block my-2 text-warning" />
                <div className="font-weight-bold font-size-lg text-black">
                  Sözleşmeler
                </div>
                <div className="font-size-sm mb-1 text-black-50">
                  Monthly Stats
                </div>
              </div>
            </button>
          </div>
          <div className="w-50 p-2">
            <button className="btn card card-box text-left d-flex justify-content-center px-4 py-3 w-100">
              <div>
                <Briefcase className="h1 d-block my-2 text-success" />
                <div className="font-weight-bold font-size-lg text-black">
                  Teklifler
                </div>
                <div className="font-size-sm mb-1 text-black-50">
                  Customers stats
                </div>
              </div>
            </button>
          </div>
          <div className="w-50 p-2">
            <button className="btn card card-box text-left d-flex justify-content-center px-4 py-3 w-100">
              <div>
                <Users className="h1 d-block my-2 text-danger" />
                <div className="font-weight-bold font-size-lg text-black">
                  Projeler
                </div>
                <div className="font-size-sm mb-1 text-black-50">
                  Manage servers
                </div>
              </div>
            </button>
          </div>
          <div className="w-50 p-2">
            <button className="btn card card-box text-left d-flex justify-content-center px-4 py-3 w-100">
              <div>
                <Layers className="h1 d-block my-2 text-white" />
                <div className="font-weight-bold font-size-lg text-black">
                  Tasks
                </div>
                <div className="font-size-sm mb-1 text-black-50">
                  Pending items
                </div>
              </div>
            </button>
          </div>
        </div>
      </Card>
    </Fragment>
  );
};

export default MusteriInfo;
