import React, { Fragment } from 'react';
import { Avatar, Button, IconButton } from '@material-ui/core';
import { useMusteriKontakKisiler } from '../../../services/ApiService';
import { MoreVertical } from 'react-feather';
import { getFirstLetters } from 'utils/kisiUtils';
const MusteriKisilerTable = props => {
  const { musteri } = props;

  const { kisiler, isLoading, isError } = useMusteriKontakKisiler(musteri._id);
  if (!kisiler) {
    return <></>;
  }

  return (
    <Fragment>
      <div className="table-responsive">
        <table className="table table-striped table-hover text-nowrap mb-0">
          <thead className="thead-light">
            <tr>
              <th className="text-left">Yetkili Kişi</th>
              <th className="text-left"></th>
            </tr>
          </thead>
          <tbody>
            {kisiler.map(k => (
              <tr>
                <td>
                  <div className="d-flex align-items-center">
                    <Avatar alt="..." className="mr-2 bg-info">
                      {getFirstLetters(k.adi, k.soyadi)}
                    </Avatar>
                    <div className=" text-left">
                      <a
                        href="#/"
                        onClick={e => e.preventDefault()}
                        className="font-weight-bold text-black"
                        title="...">
                        {k.adi} {k.soyadi}
                      </a>
                      <span className="text-black-50 d-block text-left">
                        {k.unvani}
                      </span>
                    </div>
                  </div>
                </td>
                <td>
                  <IconButton>
                    <MoreVertical />
                  </IconButton>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
};

export default MusteriKisilerTable;
