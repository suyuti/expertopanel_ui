import React, { Fragment } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Grid, Card } from '@material-ui/core';

export default function DashboardSatisSection1() {
    return (
      <Fragment>
        <Grid container spacing={4}>
          <Grid item xs={12} sm={6} lg={3}>
            <Card className="card-box border-0 card-shadow-danger p-4 mb-4">
              <div className="d-flex align-items-center">
                <div className="d-40 rounded-circle bg-danger text-white text-center font-size-lg mr-3">
                  <FontAwesomeIcon icon={['far', 'user']} />
                </div>
                <div className="text-black-50">Müşterilerim</div>
              </div>
              <div className="display-3 text-center line-height-sm text-second text-center d-flex align-items-center pt-3 justify-content-center">
                <FontAwesomeIcon
                  icon={['fas', 'arrow-up']}
                  className="font-size-sm text-success mr-2"
                />
                <div>0</div>
              </div>
              <div className="text-black-50 text-center opacity-6 pt-3">
                <b>0%</b> son ay
              </div>
            </Card>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
  