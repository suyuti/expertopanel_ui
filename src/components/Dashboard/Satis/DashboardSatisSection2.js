import React, { Fragment, useState, useEffect } from 'react';

import { Grid, LinearProgress, Card, Button } from '@material-ui/core';

import CountUp from 'react-countup';
import axios from 'axios';
import { getGrupUyeleri } from 'services/graphGroupService';
import {useDispatch} from 'react-redux'
import Gorevlerim from '../../../components/Gorev/Gorevlerim'

function act(me) {
  return {
    type: 'AAA',
    me: me
  }
}

export default function DashboardSatisSection2() {
  const dispatch = useDispatch()
  const [u, setU] = useState(null)
  useEffect(() => {
    const load = async () => {
      //getGrupUyeleri('').then(r => {
        setU('r')
      //  dispatch(act(r))
      //})
    }
    load()
  }, [])

  if (!u) {
    return <>loading</>
  }
  return (
    <Fragment>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={4}>
          <Card className="card-box mb-4 p-3">
            <div className="d-flex align-items-center pb-4 justify-content-between">
              <div>
                <div className="font-weight-bold">Ciro</div>
                <span className="text-black-50 d-block">Aylık ciro</span>
              </div>
              <div>
                <div className="display-4 font-weight-bold text-second">
                  <CountUp
                    start={0}
                    end={0}
                    duration={2}
                    deplay={2}
                    separator=""
                    decimals={3}
                    decimal=","
                  />
                </div>
                <div className="display-6  text-second"> TL</div>
              </div>
            </div>

            <div>
              <LinearProgress
                color="primary"
                variant="determinate"
                value={1}
              />
              <div className="align-box-row progress-bar--label mt-2 text-muted">
                <div>Hedef</div>
                <div className="ml-auto">100%</div>
              </div>
            </div>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
}
