import React, { useEffect, useState } from 'react';
import MaterialTable from 'material-table';
import { connect } from 'react-redux';
import useSWR from 'swr';
import axios from 'axios';
import {getEvents, getMails} from '../../services/GraphService'

const ToplantilarTable = props => {
  const token = window.localStorage.getItem('token')
  const [events, setEvents] = useState([])

  useEffect(() => {
    getMails(token).then(e => 
        {
            setEvents(e.value)}
    )    
  }, [])

  if (!events) {
      return <></>
  }
  return (
    <MaterialTable
      title="Toplantilar"
      //isLoading={!data}
      data={events}
      columns={[
        { title: 'Konu', field: 'subject' },
        { title: 'Sender', field: 'from.emailAddress.name' }
    ]}
      options={{
        pageSize: 100
      }}
      onRowClick={(e, rowData) => props.history.push(`/musteriler/${rowData._id}`)}
    />
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ToplantilarTable);
