import React, { Fragment } from 'react';
import { login } from '../../services/AuthService';
//import { login } from '../../services/ApiService';

import {
  Grid,
  Fab,
  Container,
  InputAdornment,
  Drawer,
  IconButton,
  Card,
  CardContent,
  Button,
  List,
  ListItem,
  Tooltip,
  TextField,
  Divider
} from '@material-ui/core';

import MailOutlineTwoToneIcon from '@material-ui/icons/MailOutlineTwoTone';

import projectLogo from '../../assets/images/experto-logo-x.png';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Link } from 'react-router-dom';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';

import svgImage6 from '../../assets/images/illustrations/data_points.svg';

import svgImage11 from '../../assets/images/illustrations/businesswoman.svg';

import hero6 from '../../assets/images/hero-bg/hero-2.jpg';

import IconsFeather from '../../example-components/Icons/IconsFeather';
import IconsIon from '../../example-components/Icons/IconsIon';
import IconsPe7 from '../../example-components/Icons/IconsPe7';
import IconsFontawesome from '../../example-components/Icons/IconsFontawesome';

const LandingPage = () => {
  const [state, setState] = React.useState({
    right: false
  });
  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };
  return (
    <Fragment>
      <div className="hero-wrapper bg-composed-wrapper bg-white">
        <div className="header-nav-wrapper header-nav-wrapper-lg w-100 navbar-dark">
          <Container className="d-flex" fixed>
            <div className="header-nav-logo align-items-center d-flex justify-content-start">
              <div className="nav-logo">
                <Link
                  to="/DashboardDefault"
                  title="Carolina React Admin Dashboard with Material-UI PRO">
                  <i className="bg-white">
                    <img
                      alt="Carolina React Admin Dashboard with Material-UI PRO"
                      src={projectLogo}
                    />
                  </i>
                  <span>Experto</span>
                </Link>
              </div>
            </div>
            <div className="header-nav-menu d-none d-lg-block">
              <div className="d-flex justify-content-center text-white">
                <Button
                  color="inherit"
                  className="btn-inverse px-3 mx-1 py-2 text-capitalize"
                  component={Link}
                  to="/DashboardDefault">
                  Ürünlerimiz
                </Button>
                <Button
                  color="inherit"
                  className="btn-inverse px-3 mx-1 py-2 text-capitalize"
                  component={Link}
                  to="/ApplicationsChat">
                  Kampanyalar
                </Button>
              </div>
            </div>
            <div className="header-nav-actions flex-grow-0 flex-lg-grow-1">
              <span className="d-none d-lg-block">
                <Button
                  onClick={login}
                  //component={Link}
                  //to="/DashboardDefault"
                  className="px-3"
                  color="primary"
                  variant="contained">
                  Giriş.
                </Button>
              </span>
              <span className="d-block d-lg-none">
                <Fab
                  onClick={toggleDrawer('right', true)}
                  color="secondary"
                  size="medium">
                  <MenuRoundedIcon />
                </Fab>
              </span>
            </div>
            <Drawer
              variant="temporary"
              anchor="right"
              open={state.right}
              onClose={toggleDrawer('right', false)}
              elevation={11}>
              <List className="py-0">
                <ListItem className="d-block bg-secondary py-2 px-3">
                  <div className="d-flex w-100 justify-content-between navbar-light align-content-center">
                    <div className="header-nav-logo justify-content-start">
                      <a
                        href="#/"
                        onClick={e => e.preventDefault()}
                        className="navbar-brand d-flex align-items-center d-40"
                        title="Carolina React Admin Dashboard with Material-UI PRO">
                        <img
                          alt="Carolina React Admin Dashboard with Material-UI PRO"
                          className="d-block img-fluid"
                          src={projectLogo}
                        />
                      </a>
                    </div>
                    <IconButton
                      onClick={toggleDrawer('right', false)}
                      color="primary">
                      <MenuRoundedIcon />
                    </IconButton>
                  </div>
                </ListItem>
                <Divider />
                <ListItem className="d-block py-3 px-2">
                  <Link
                    to="/DashboardDefault"
                    className="d-flex px-2 align-items-center dropdown-item rounded">
                    <div className="align-box-row w-100">
                      <div className="mr-3">
                        <div className="bg-deep-blue text-center text-white d-40 rounded-circle">
                          <FontAwesomeIcon icon={['fas', 'object-group']} />
                        </div>
                      </div>
                      <div className="text-truncate max-w-70 overflow-hidden">
                        <div className="font-weight-bold text-primary d-block">
                          Dashboards
                        </div>
                        <span className="text-black-50">
                          12 different dashboards to choose from
                        </span>
                      </div>
                      <div className="ml-auto card-hover-indicator align-self-center">
                        <FontAwesomeIcon
                          icon={['fas', 'chevron-right']}
                          className="font-size-lg"
                        />
                      </div>
                    </div>
                  </Link>
                </ListItem>
                <Divider />
                <ListItem className="d-block py-3 px-2">
                  <Link
                    to="/ApplicationsChat"
                    className="d-flex px-2 align-items-center dropdown-item rounded">
                    <div className="align-box-row w-100">
                      <div className="mr-3">
                        <div className="bg-strong-bliss text-center text-white d-40 rounded-circle">
                          <FontAwesomeIcon icon={['fas', 'sitemap']} />
                        </div>
                      </div>
                      <div className="text-truncate max-w-70 overflow-hidden">
                        <div className="font-weight-bold text-primary d-block">
                          Apps
                        </div>
                        <span className="text-black-50">
                          Multiple application designs included
                        </span>
                      </div>
                      <div className="ml-auto card-hover-indicator align-self-center">
                        <FontAwesomeIcon
                          icon={['fas', 'chevron-right']}
                          className="font-size-lg"
                        />
                      </div>
                    </div>
                  </Link>
                </ListItem>
                <Divider />
                <ListItem className="d-block py-3 px-2">
                  <Link
                    to="/Cards3"
                    className="d-flex px-2 align-items-center dropdown-item rounded">
                    <div className="align-box-row w-100">
                      <div className="mr-3">
                        <div className="bg-arielle-smile text-center text-white d-40 rounded-circle">
                          <FontAwesomeIcon icon={['fas', 'shapes']} />
                        </div>
                      </div>
                      <div className="text-truncate max-w-70 overflow-hidden">
                        <div className="font-weight-bold text-primary d-block">
                          Cards
                        </div>
                        <span className="text-black-50">
                          Over 300 different cards available
                        </span>
                      </div>
                      <div className="ml-auto card-hover-indicator align-self-center">
                        <FontAwesomeIcon
                          icon={['fas', 'chevron-right']}
                          className="font-size-lg"
                        />
                      </div>
                    </div>
                  </Link>
                </ListItem>
                <Divider />
                <ListItem className="d-block py-3 px-2">
                  <Link
                    to="/RegularTables"
                    className="d-flex px-2 align-items-center dropdown-item rounded">
                    <div className="align-box-row w-100">
                      <div className="mr-3">
                        <div className="bg-happy-green text-center text-white d-40 rounded-circle">
                          <FontAwesomeIcon icon={['fas', 'table']} />
                        </div>
                      </div>
                      <div className="text-truncate max-w-70 overflow-hidden">
                        <div className="font-weight-bold text-primary d-block">
                          Tables
                        </div>
                        <span className="text-black-50">
                          Multiple, easy to customise tables
                        </span>
                      </div>
                      <div className="ml-auto card-hover-indicator align-self-center">
                        <FontAwesomeIcon
                          icon={['fas', 'chevron-right']}
                          className="font-size-lg"
                        />
                      </div>
                    </div>
                  </Link>
                </ListItem>
                <Divider />
                <ListItem className="d-block py-3 px-2">
                  <Link
                    to="/FormsWizard"
                    className="d-flex px-2 align-items-center dropdown-item rounded">
                    <div className="align-box-row w-100">
                      <div className="mr-3">
                        <div className="bg-skim-blue text-center text-white d-40 rounded-circle">
                          <FontAwesomeIcon icon={['fas', 'align-center']} />
                        </div>
                      </div>
                      <div className="text-truncate max-w-70 overflow-hidden">
                        <div className="font-weight-bold text-primary d-block">
                          Forms
                        </div>
                        <span className="text-black-50">
                          Over 50 forms elements included
                        </span>
                      </div>
                      <div className="ml-auto card-hover-indicator align-self-center">
                        <FontAwesomeIcon
                          icon={['fas', 'chevron-right']}
                          className="font-size-lg"
                        />
                      </div>
                    </div>
                  </Link>
                </ListItem>
                <Divider />
                <ListItem className="d-block py-3 px-2">
                  <Button
                    href="/DashboardDefault"
                    className="text-white w-100"
                    variant="contained"
                    color="secondary">
                    View Live Demo
                  </Button>
                </ListItem>
              </List>
            </Drawer>
          </Container>
        </div>

        <div className="flex-grow-1 w-100 d-flex align-items-center">
          <div
            className="bg-composed-wrapper--image bg-composed-filter-rm opacity-9"
            style={{ backgroundImage: 'url(' + hero6 + ')' }}
          />
          <div className="bg-composed-wrapper--content pt-5 pb-2 py-lg-5">
            <Container fixed className="pb-5">
              <Grid container spacing={4}>
                <Grid
                  item
                  xs={12}
                  lg={7}
                  xl={6}
                  className="d-flex align-items-center">
                  <div>
                    <div className="d-flex align-items-center">
                      <Tooltip
                        arrow
                        placement="right"
                        title=".">
                        <span className="text-black-50 pl-2">
                          <FontAwesomeIcon icon={['far', 'question-circle']} />
                        </span>
                      </Tooltip>
                    </div>
                    <div className="text-black mt-3">
                      <h1 className="display-2 mb-3 font-weight-bold">
                        EXPERTO
                      </h1>
                      <p className="font-size-lg text-black-50">
                        ...
                      </p>
                      <p className="text-black">
                      </p>
                      <div className="divider border-2 border-dark mt-4 mb-2 border-light opacity-2 rounded-circle w-25" />
                      <div>
                        <Button
                          to="/DashboardDefault"
                          component={Link}
                          size="large"
                          className="py-3 px-5 mt-2"
                          color="primary"
                          variant="contained"
                          title="View Carolina React Admin Dashboard with Material-UI PRO Live Preview">
                          <span className="btn-wrapper--label">Giriş</span>
                          <span className="btn-wrapper--icon">
                            <FontAwesomeIcon icon={['fas', 'arrow-right']} />
                          </span>
                        </Button>
                      </div>
                      <small className="d-block pt-3">
                      </small>
                    </div>
                  </div>
                </Grid>
                <Grid
                  item
                  xs={12}
                  lg={5}
                  xl={6}
                  className="px-0 d-none d-lg-flex align-items-center">
                </Grid>
              </Grid>
            </Container>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default LandingPage;
