export const config = {
    appId: process.env.REACT_APP_APP_ID,
    redirectUri: process.env.REACT_APP_REDIRECT_URI,
    scopes: [
        // https://docs.microsoft.com/en-us/graph/permissions-reference#user-permissions
 /*       'User.Read',
        'User.Read.All',
        'User.ReadBasic.All',
        'Group.Read.All',
        //'group.read',
        //'directory.read',
        //'group.read',
        'calendars.read',
        'Mail.Read',
        'Mail.ReadBasic'
*/

        'User.ReadBasic.All',
        'User.Read',
        'User.ReadWrite',
        'User.Read.All',
        'User.ReadWrite.All',

        'Mail.Read',
        'Mail.ReadBasic',
        'Mail.ReadWrite.Shared',

        'Directory.Read.All',
        'Directory.ReadWrite.All',
        'Directory.AccessAsUser.All',
    ]
};
