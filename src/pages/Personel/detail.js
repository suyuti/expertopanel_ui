import React, { Fragment, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { PageTitle } from 'layout-components';
import {
  Grid,
  Card,
  Box,
  Tabs,
  Tab,
  Typography,
  CardHeader,
  Divider,
  IconButton,
  CardContent,
  Tooltip
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Chart from 'react-apexcharts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import CountUp from 'react-countup';
import SatisPerformans from '../../components/Personel/SatisPerformans'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      {...other}>
      {value === index && <Box p={0}>{children}</Box>}``
    </Typography>
  );
}

const PersonelDetail = props => {
  const personeller = useSelector(state => state.ThemeOptions.users);
  const [personel, setPersonel] = useState(null);
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const chart3Options = {
    chart: {
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      stacked: true
    },
    dataLabels: {
      enabled: false
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '90%'
      }
    },
    colors: ['#4191ff', 'rgba(65, 145, 255, 0.35)'],
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    fill: {
      opacity: 1
    },
    legend: {
      show: false
    },
    labels: [
      'Ocak 2020',
      'Şubat 2020',
      'Mart 2020',
      'Nisan 2020',
      'Mayıs 2020',
      'Haziran 2020',
      'Temmuz 2020'
    ],
    xaxis: {
      crosshairs: {
        width: 1
      }
    },
    yaxis: {
      min: 0
    }
  };
  const chart3Data = [
    {
      name: 'Gerçekleşen',
      data: [2.3, 3.1, 4.0, 3.8, 5.1, 3.6, 4.0, 3.8, 5.1, 3.6, 3.2]
    },
    {
      name: 'Satış Hedefi',
      data: [2.1, 2.1, 3.0, 2.8, 4.0, 3.8, 5.1, 3.6, 4.1, 2.6, 1.2]
    }
  ];

  useEffect(() => {
    const load = async () => {
      if (personeller) {
        const p = personeller.find(p => p.id == props.match.params.id);
        setPersonel(p);
      }
    };
    load();
  }, []);

  if (!personel) {
    return <></>;
  }

  return (
    <Fragment>
      <PageTitle
        titleHeading="Profil"
        titleDescription="Kullanıcı profil sayfası"
      />
      <Grid container spacing={4}>
        <Grid item xs={12} lg={3}>
          <div className="bg-secondary p-3 border rounded  h-100">
            <div className="d-flex align-items-start justify-content-between">
              <div className="avatar-icon-wrapper d-100">
                <div className="avatar-icon d-100">
                  <img alt="..." src={personel.photo} />
                </div>
              </div>
            </div>
            <div className="font-weight-bold font-size-lg d-flex align-items-center mt-2 mb-0">
              <span>{personel.displayName}</span>
            </div>
          </div>
        </Grid>
        <Grid item xs={12} lg={8}>
          <Tabs
            value={value}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            onChange={handleChange}>
            <Tab label="Performans" />
            <Tab label="Aktivite" />
            <Tab label="Ayarlar" />
          </Tabs>
          <TabPanel value={value} index={0}>
            <Grid container spacing={4}>
              <Grid item xs={12} md={6} lg={6}>
              <SatisPerformans />
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={value} index={1}>
          </TabPanel>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default PersonelDetail;
