import React, { Fragment } from 'react';
import MaterialTable from 'material-table';
import { useSelector } from 'react-redux';
import { Avatar } from '@material-ui/core';

export default function Personeller(props) {
  const personeller = useSelector(state => state.ThemeOptions.users);
  return (
    <Fragment>
      <MaterialTable
        title="Personeller"
        data={personeller}
        columns={[
          {
            title: 'Personel',
            render: user => (
              <Fragment>
                <div className="d-flex align-items-center">
                  <Avatar alt="..." src={user.photo} className="mr-2" />
                  <div>
                    <a
                      href="#/"
                      onClick={e => e.preventDefault()}
                      className="font-weight-bold text-black"
                      title="...">
                      {user.displayName}
                    </a>
                    <span className="text-black-50 d-block">
                      {user.jobTitle}
                    </span>
                  </div>
                </div>
              </Fragment>
            )
          }
        ]}
        onRowClick={(e, rowData) => props.history.push(`/personeller/${rowData.id}`)}

        options={{
          pageSize: 100
        }}
      />
    </Fragment>
  );
}
