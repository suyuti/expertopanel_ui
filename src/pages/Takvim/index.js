import React, { Fragment } from 'react';

import ApplicationsCalendarContent from '../../example-components/ApplicationsCalendar/ApplicationsCalendarContent';
export default function TakvimPage() {
  return (
    <Fragment>
      <ApplicationsCalendarContent />
    </Fragment>
  );
}
