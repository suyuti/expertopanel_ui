import React, { Fragment, useEffect } from 'react';
import MaterialTable from 'material-table';
import { useSelector, useDispatch } from 'react-redux';
import { getGorevlerim } from '../../actions/me.actions';
import moment from 'moment';
import { Avatar } from '@material-ui/core';

const GorevlerPage = props => {
  const dispatch = useDispatch();
  const gorevlerim = useSelector(state => state.ThemeOptions.gorevlerim);
  const me = useSelector(state => state.ThemeOptions.me);
  const personeller = useSelector(state => state.ThemeOptions.users);

  useEffect(() => {
    const load = async () => {
      if (me) {
        dispatch(getGorevlerim(me.id));
      }
    };
    load();
  }, [dispatch]);

  if (!gorevlerim) {
    return <></>;
  }

  return (
    <Fragment>
      <MaterialTable
        title="Görevlerim"
        data={gorevlerim}
        columns={[
          { title: 'Görev', field: 'title' },
          {
            title: 'Başlangıç',
            field: 'startDateTime',
            render: rowData =>
              rowData.startDateTime
                ? moment(rowData.startDateTime).format('DD.MM.YYYY')
                : ''
          },
          {
            title: 'Bitiş',
            field: 'dueDateTime',
            render: rowData =>
              rowData.dueDateTime
                ? moment(rowData.dueDateTime).format('DD.MM.YYYY')
                : ''
          },
          {
            title: 'Atama',
            render: rowData => {
              return (
                <div>
                  {Object.keys(rowData.assignments).map(a => {
                    let personel = personeller.find(p => p.id == a);
                    if (personel) {
                      return (
                        <Fragment>
                          <div className="d-flex align-items-center">
                            <Avatar
                              alt="..."
                              src={personel.photo}
                              className="mr-2"
                            />
                            <div>
                              <a
                                //href="#/"
                                onClick={e => e.preventDefault()}
                                className="font-weight-bold text-black"
                                title="...">
                                {personel.displayName}
                              </a>
                              <span className="text-black-50 d-block">
                                {personel.jobTitle}
                              </span>
                            </div>
                          </div>
                        </Fragment>
                      );
                    }
                  })}
                </div>
              );
            }
          }
        ]}
        onRowClick={(e, rowData) => props.history.push(`/gorevler/${rowData.id}`)}
        />
    </Fragment>
  );
};

export default GorevlerPage;
