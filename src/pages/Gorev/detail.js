import React, { Fragment, useState } from 'react';
import { Grid, TextField } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

const GorevDetail = props => {
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(false);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={6} className="p-4">
          <TextField
            label="Görev"
            className="m-2"
            fullWidth
            variant={editMode ? 'outlined' : 'standard'}
            InputProps={{ readOnly: !editMode }}
            //value={musteri.iletisim.adres}
            onChange={e => {
              //var m = { ...musteri };
              //m.iletisim.adres = e.target.value;
              //dispatch({ type: SET_MUSTERI, musteri: m });
            }}
          />
          <Grid container>
            <Grid item xs={6} className="p-4">
              <TextField
                label="Baslangic"
                className="m-2"
                fullWidth
                variant={editMode ? 'outlined' : 'standard'}
                InputProps={{ readOnly: !editMode }}
                //value={musteri.iletisim.adres}
                onChange={e => {
                  //var m = { ...musteri };
                  //m.iletisim.adres = e.target.value;
                  //dispatch({ type: SET_MUSTERI, musteri: m });
                }}
              />
            </Grid>
            <Grid item xs={6} className="p-4">
              <TextField
                label="Bitis"
                className="m-2"
                fullWidth
                variant={editMode ? 'outlined' : 'standard'}
                InputProps={{ readOnly: !editMode }}
                //value={musteri.iletisim.adres}
                onChange={e => {
                  //var m = { ...musteri };
                  //m.iletisim.adres = e.target.value;
                  //dispatch({ type: SET_MUSTERI, musteri: m });
                }}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};
export default GorevDetail;
