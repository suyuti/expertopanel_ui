import React, { Fragment } from 'react';

import { PageTitle } from '../../layout-components';

export default function KisiDetailPage() {
  return (
    <Fragment>
      <PageTitle
        titleHeading="Kontak Kisi"
        titleDescription="Kisi"
      />
    </Fragment>
  );
}
