import React, { Fragment } from 'react';

import { PageTitle } from '../../layout-components';
import KisilerTable from '../../components/KisilerTable'
//import PagesProfileContent from '../../example-components/PagesProfile/PagesProfileContent';
export default function PagesProfile(props) {
  return (
    <Fragment>
      <PageTitle
        titleHeading="Kişiler"
        titleDescription="Kişiler"
      />
      <KisilerTable {...props}/>

    </Fragment>
  );
}
