import React, { Fragment, useEffect } from 'react';
import { PageTitle } from '../../layout-components';
import UrunlerTable from '../../components/UrunlerTable'
import { useDispatch, useSelector } from 'react-redux';
import { getUrunler } from 'actions/urunler.actions';

export default function UrunlerPage(props) {
  const dispatch = useDispatch()
  const urunler = useSelector(state => state.ThemeOptions.urunler)
  useEffect(() => {
    const load = async () => {
      dispatch(getUrunler())
    }
    load()
  }, [dispatch])

  if (!urunler) {
    return <></>
  }

  return (
    <Fragment>
      <PageTitle
        titleHeading="Ürünler"
        titleDescription="Ürünler"
      />
      <UrunlerTable {...props} />

    </Fragment>
  );
}
