import React, { Fragment } from 'react';

import { PageTitle } from '../../layout-components';
import UrunInfo from 'components/Urun/UrunInfo';

export default function UrunDetailPage(props) {
  return (
    <Fragment>
      <PageTitle
        titleHeading="Ürün Detay"
        titleDescription="Ürün"
      />
      <UrunInfo {...props}/>
    </Fragment>
  );
}
