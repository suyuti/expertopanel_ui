import React, { Fragment } from 'react';
///import { login } from '../../services/AuthService';
//import { login } from '../../services/ApiService';
import { login } from '../../actions/me.actions';

import {
  Grid,
  Fab,
  Container,
  InputAdornment,
  Drawer,
  IconButton,
  Card,
  CardContent,
  Button,
  List,
  ListItem,
  Tooltip,
  TextField,
  Divider
} from '@material-ui/core';

import MailOutlineTwoToneIcon from '@material-ui/icons/MailOutlineTwoTone';

import projectLogo from '../../assets/images/experto-logo-x.png';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Link } from 'react-router-dom';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';

import svgImage6 from '../../assets/images/illustrations/data_points.svg';

import svgImage11 from '../../assets/images/illustrations/businesswoman.svg';

import hero6 from '../../assets/images/hero-bg/hero-2.jpg';

import IconsFeather from '../../example-components/Icons/IconsFeather';
import IconsIon from '../../example-components/Icons/IconsIon';
import IconsPe7 from '../../example-components/Icons/IconsPe7';
import IconsFontawesome from '../../example-components/Icons/IconsFontawesome';
import { useDispatch } from 'react-redux';

const LandingPage = props => {
  const dispatch = useDispatch();
  const [state, setState] = React.useState({
    right: false
  });
  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const redirect = url => {
    props.history.push(url);
  };

  return (
    <Fragment>
      <div className="app-wrapper bg-light">
        <div className="app-main">
          <div className="app-content p-0">
            <div className="app-inner-content-layout--main">
              <div className="flex-grow-1 w-100 d-flex align-items-center">
                <div className="bg-composed-wrapper--content">
                  <div className="hero-wrapper bg-composed-wrapper min-vh-100">
                    <div className="flex-grow-1 w-100 d-flex align-items-center">
                      <Grid
                        item
                        lg={6}
                        md={9}
                        className="px-4 mx-auto text-center text-black">
                        <h1 className="display-1 mb-3 px-4 font-weight-bold">
                          Experto
                        </h1>
                        <Button
                          onClick={() => {
                            dispatch(login(redirect));
                          }}
                          size="large"
                          color="primary"
                          variant="contained"
                          className="text-white mt-4">
                          <span className="btn-wrapper--label">Giriş</span>
                        </Button>
                      </Grid>
                    </div>
                    <div className="hero-footer py-4"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default LandingPage;
