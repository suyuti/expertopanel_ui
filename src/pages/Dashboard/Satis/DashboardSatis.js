import React, { Fragment } from "react"
import { PageTitle } from '../../../layout-components';

import DashboardSatisSection1 from '../../../components/Dashboard/Satis/DashboardSatisSection1'
import DashboardSatisSection2 from '../../../components/Dashboard/Satis/DashboardSatisSection2'

const DashboardSatis = (props) => {
    return (
        <Fragment>
        <PageTitle
            titleHeading="Satış Masası"
            titleDescription="Satış masası"
        />
        <DashboardSatisSection2 />
        <DashboardSatisSection1 />
        </Fragment>
    )
}

export default DashboardSatis