import React, { Fragment } from 'react';

import { PageTitle } from '../../layout-components';
import ToplantilarTable from '../../components/ToplantilarTable'
//import PagesProfileContent from '../../example-components/PagesProfile/PagesProfileContent';
export default function ToplantilarPage(props) {
  return (
    <Fragment>
      <PageTitle
        titleHeading="Toplantilar"
        titleDescription="Toplantilar"
      />
      <ToplantilarTable {...props}/>

    </Fragment>
  );
}
