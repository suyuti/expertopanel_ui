import React, { Fragment } from 'react';

import { PageTitle } from '../../layout-components';
import MusterilerTable from '../../components/Musteri/MusterilerTable'
//import PagesProfileContent from '../../example-components/PagesProfile/PagesProfileContent';
const queryString = require('query-string');

function MusteriDurumAdi(props) {
  const params = queryString.parse(props.location.search)
  switch (params.durum) {
    case 'aktif': return 'Aktif';
    case 'pasif': return 'Pasif';
    case 'potansiyel': return 'Potansiyel';
    case 'sorunlu': return 'Sorunlu';
    default: return '';
  }
}

export default function MusterilerPage(props) {

  return (
    <Fragment>
      <PageTitle
        titleHeading={`${MusteriDurumAdi(props)} Müşteriler`}
        titleDescription="Müşteri listesi"
      />
      <MusterilerTable {...props}/>

    </Fragment>
  );
}

