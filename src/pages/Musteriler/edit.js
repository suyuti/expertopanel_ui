import React, { Fragment } from 'react';

import { PageTitle } from '../../layout-components';
import { Grid, Drawer, Box } from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';

import MusteriInfo from '../../components/Musteri/MusteriInfo';
import MusteriTimeline from 'components/Musteri/MusteriTimeline';
import MusteriMailler from 'components/Musteri/MusteriMailler';
import useSWR from 'swr';
import { connect } from 'react-redux';
import axios from 'axios';
import { useMusteri } from 'services/ApiService';
import MusteriForm from 'components/Musteri/MusteriForm';
import MusteriIletisimForm from 'components/Musteri/MusteriIletisimForm';
import MusteriPageHeader from 'components/Musteri/MusteriPageHeader'
import {useGroup} from '../../services/GraphService'

const  MusteriEditPage = (props) => {
  const {musteri, isLoading, isError} = useMusteri(props.match.params.id);
  const [group, setGroup] = React.useState(null);
  const token = window.localStorage.getItem('token');

  useGroup(token, 'Satış').then(g => setGroup(g))

  if (!musteri || !group) {
    return <></>
  }

  return (
    <Fragment>
      <MusteriPageHeader titleHeading='Musteri Güncelleme' titleDescription='musteri' musteri={musteri} group={group} />
     

      {musteri && <Grid container spacing={4}>

      <Grid item xs={12} lg={4}>
          <MusteriForm musteri={musteri} />
        </Grid>
        <Grid item xs={12} lg={4}>
          <MusteriIletisimForm musteri={musteri}/>
        </Grid>
        
      </Grid>}
    </Fragment>
  );
}

const mapStateToProps = state => ({
  musteri: state.ThemeOptions.musteri,
});

const mapDispatchToProps = dispatch => ({
  //setSidebarToggleMobile: enable => dispatch(setSidebarToggleMobile(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MusteriEditPage);
