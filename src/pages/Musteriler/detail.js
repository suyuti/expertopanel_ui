import React, { Fragment, useEffect, useState } from 'react';

import { PageTitle } from '../../layout-components';
import {
  Grid,
  Tab,
  Drawer,
  Box,
  Tabs,
  CircularProgress
} from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';

import MusteriInfo from '../../components/Musteri/MusteriInfo';
import MusteriTimeline from 'components/Musteri/MusteriTimeline';
import MusteriMailler from 'components/Musteri/MusteriMailler';
import useSWR from 'swr';
import { connect, useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { useMusteri } from 'services/ApiService';
import MusteriForm from 'components/Musteri/MusteriForm';
import MusteriPageHeader from 'components/Musteri/MusteriPageHeader';
import { useGroup } from '../../services/GraphService';
//import { getMusteri } from 'services/expertoService';
import { getGroupByName } from 'services/graph_Service';
import MusteriAdres from 'components/Musteri/MusteriAdres';
import MusteriPage from 'components/Musteri/MusteriPage';
import MusteriKisiler from 'components/Musteri/MusteriKisiler';
import MusteriIletisim from 'components/Musteri/MusteriIletisim';
import MusteriMaliBilgiler from 'components/Musteri/MusteriMaliBilgiler';
import { makeStyles } from '@material-ui/core/styles';
import {getMusteri, getSektorler} from 'actions/musteriler.actions'
import MusteriYazismalar from 'components/Musteri/MusteriYazismalar';
import MusteriTeklifler from 'components/Musteri/MusteriTeklifler';
import MusteriSozlesmeler from 'components/Musteri/MusteriSozlesmeler';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`
  }
}));

const MusteriDetailPage = props => {
  const classes = useStyles();
  //const [musteri, setMusteri] = useState(null);
  const [value, setValue] = useState(0);
  const [group, setGroup] = useState(null);
  const [selectedTab, setSelectedTab] = useState(0);
  const dispatch = useDispatch()
  const musteri = useSelector(state => state.ThemeOptions.musteri)

  useEffect(() => {
    const load = async () => {
      dispatch(getMusteri(props.match.params.id))
      dispatch(getSektorler())



      //getMusteri(props.match.params.id).then(musteri => setMusteri(musteri));
      //getGroupByName('EXPERTO TEAM').then(resp => {
      //  setGroup(resp);
      //});
    };
    load();
  }, [dispatch]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  if (!musteri) {
    return <></>;
  }

  return (
    <Fragment>
      <MusteriPageHeader
        titleHeading="Musteri Detay"
        titleDescription="Müşteri Detay Bilgiler"
        musteri={musteri}
        group={group}
      />
      <Grid container>
        <Grid item xs={2}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={selectedTab}
            onChange={(event, newValue) => setSelectedTab(newValue)}
            className={classes.tabs}>
            <Tab label="Genel bilgiler" />
            <Tab label="İletişim" />
            <Tab label="Kişiler" />
            <Tab label="Mali Bilgiler" />
            <Tab label="Teklifler" />
            <Tab label="Sözleşmeler" />
            <Tab label="Yazışmalar" />
          </Tabs>
        </Grid>
        <Grid item xs={10}>
          {selectedTab == 0 && 
            <MusteriPage {...props} />
          }
          {selectedTab == 1 && 
            <MusteriIletisim {...props} />
          }
          {selectedTab == 2 && 
            <MusteriKisiler  {...props} />
          }
          {selectedTab == 3 && 
            <MusteriMaliBilgiler {...props} />
          }
          {selectedTab == 4 && 
            <MusteriTeklifler {...props} />
          }
          {selectedTab == 5 && 
            <MusteriSozlesmeler {...props} />
          }
          {selectedTab == 6 && 
            <MusteriYazismalar {...props} />
          }
        </Grid>
      </Grid>

      <Grid container spacing={4}>
        {false && (
          <>
            <Grid item xs={12} lg={4}>
              <MusteriInfo musteri={musteri} {...props} />
            </Grid>
            <Grid item xs={12} lg={4}>
              <MusteriAdres musteri={musteri} {...props} />
            </Grid>
            <Grid item xs={12} lg={8}>
              <Tabs
                value={value}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                onChange={handleChange}>
                <Tab label="Bilgi"></Tab>
                <Tab label="Aktivite"></Tab>
              </Tabs>
              {value == 0 && <></>
              /*
            <MusteriMailler />
            */
              }
              {value == 1 && <MusteriTimeline />}
            </Grid>
          </>
        )}
      </Grid>

      {musteri && false && (
        <Grid container spacing={4}>
          <Grid item xs={12} lg={4}>
            <MusteriInfo musteri={musteri} {...props} />
          </Grid>

          <Grid item xs={12} lg={4}>
            <MusteriAdres musteri={musteri} {...props} />
          </Grid>

          <Grid item xs={12} lg={4}>
            <MusteriTimeline />
          </Grid>
          <Grid item xs={12} lg={4}>
            <MusteriMailler />
          </Grid>
        </Grid>
      )}
    </Fragment>
  );
};

const mapStateToProps = state => ({
  //musteri: state.ThemeOptions.musteri,
});

const mapDispatchToProps = dispatch => ({
  //setSidebarToggleMobile: enable => dispatch(setSidebarToggleMobile(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MusteriDetailPage);
