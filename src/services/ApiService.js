import axios from "axios"
import useSWR from "swr"
import { makeQueryString } from "utils/queryString"
const queryString = require('query-string')
var qs = require('qs')
const BASE_URL = process.env.REACT_APP_BASE_URL
//const https = require('https')
//const fs = require('fs')

/*const httpsAgent = new https.Agent({
    ca: fs.readFileSync('../certs/cert.pem')
})
*/

export const login = async (accessToken, uid) => {
    const token = window.localStorage.getItem('experto_access_token')
    return axios.get(`${BASE_URL}/v1/auth/signin`, 
    {
        //httpsAgent: httpsAgent,
        params: {uniqueId: uid, 
        //    token: token
        }, 
        //headers:{authorization: token}
    })
}

export const getMusteriler = async (filter, auth) => {
    await axios.get(`${BASE_URL}/v1/musteriler`,{params: {filter: filter}, headers: {authorization: auth} }).then(resp => resp.data)
}

//----------------------------------------------------------------------------------------------

export const useMusteriler = (filter) => {
    const token = window.localStorage.getItem('experto_access_token')

    const _qs = qs.stringify(filter)
    const {data, error} = useSWR(`${BASE_URL}/v1/musteri${_qs?'?'+_qs:''}`, url => 
        axios.get(url, {
            //params: {filter: filter}, 
            headers: {authorization: token}}).then(resp => resp.data)
    )
    return {
        musteriler: data,
        isLoading: !error && !data,
        isError: error
    }
}

//----------------------------------------------------------------------------------------------

export const useMusteri = (id) => {
    const token = window.localStorage.getItem('experto_access_token')
    const {data, error} = useSWR(`${BASE_URL}/v1/musteri/${id}`, url => 
        axios.get(url, {
            //params: {filter: filter}, 
            headers: {authorization: token}}).then(resp => resp.data)
    )
    return {
        musteri: data,
        isLoading: !error && !data,
        isError: error
    }
}


//----------------------------------------------------------------------------------------------

export const setMusteriTemsilcisi = async (mid, personelId) => {
    const token = window.localStorage.getItem('experto_access_token')
    const reps = await axios.patch(`${BASE_URL}/v1/musteri/${mid}`, {musteriTemsilcisi:personelId}, {
        headers: {authorization: token}
    })
}

export const useMusteriKontakKisiler = (mid) => {
    const token = window.localStorage.getItem('experto_access_token')
    const {data, error} = useSWR(`${BASE_URL}/v1/musteri/${mid}/kisiler`, url => 
        axios.get(url, {
            params: {filter: {musteri: mid}}, 
            headers: {authorization: token}}).then(resp => resp.data)
    )
    return {
        kisiler: data,
        isLoading: !error && !data,
        isError: error
    }
}

export const _useUrun = async (id) => {
    const token = window.localStorage.getItem('experto_access_token')
    let resp = await axios.get(`${BASE_URL}/v1/urun/${id}`, {
        headers: {authorization: token}})
    return resp.data

}


export const useUrun =  (id) => {
    const token = window.localStorage.getItem('experto_access_token')
    const {data, error} = useSWR(`${BASE_URL}/v1/urun/${id}`, url => 
        axios.get(url, {
            //params: {filter: filter}, 
            headers: {authorization: token}}).then(resp => resp.data)
    )
    return {
        urun: data,
        isLoading: !error && !data,
        isError: error
    }
}

export const useSektorler =  () => {
    const token = window.localStorage.getItem('experto_access_token')
    const {data, error} = useSWR(`${BASE_URL}/v1/sektor`, url => 
        axios.get(url, {
            //params: {filter: filter}, 
            headers: {authorization: token}}).then(resp => resp.data)
    )
    return {
        sektorler: data,
        isLoading: !error && !data,
        isError: error
    }
}
