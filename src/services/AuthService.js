import { UserAgentApplication } from 'msal';
import { config } from '../Config';
import store from '../config/configureStore';
import { setAuthenticated, setUser, setUserPhoto } from 'reducers/ThemeOptions';
import { getUserDetails, getUserPhoto } from './GraphService';
import {login as Login} from './ApiService'
//import { useHistory } from "react-router-dom";
//const history = useHistory()

var userAgentApplication = new UserAgentApplication({
    auth: {
        clientId: config.appId,
        redirectUri: config.redirectUri
    },
    cache: {
        cacheLocation: 'sessionStorage',
        storeAuthStateInCookie: true
    }
})

//---------------------------------------------------------------------------------

export const login = async (redirect) => {
    try {
      debugger
        userAgentApplication.loginPopup({
            scopes: config.scopes,
            prompt: 'select_account'
        }).then(authResponse => {
          getAccessToken(config.scopes).then(accessToken => 
            {
              getUserProfile().then(resp => console.log(resp))
            
            Login(accessToken, authResponse.uniqueId).then(resp => {
              window.localStorage.setItem('experto_access_token', resp.data.expertoToken)
              window.localStorage.setItem('token', accessToken)
              redirect(resp.data.redirectUrl)
            })
            }
          )
        })
        //await getUserProfile()
    }
    catch(err) {
        console.log(err)
    }
}

//---------------------------------------------------------------------------------

export const logout = async () => {
    userAgentApplication.logout()
    window.localStorage.removeItem('experto_access_token');
  }

//---------------------------------------------------------------------------------

export const getAccessToken = async scopes => {
    try {
        // Get the access token silently
        // If the cache contains a non-expired token, this function
        // will just return the cached token. Otherwise, it will
        // make a request to the Azure OAuth endpoint to get a token
        var silentResult = await userAgentApplication.acquireTokenSilent({
          scopes: scopes
        });

        return silentResult.accessToken;
      } catch (err) {
        //console.log(err)
        // If a silent request fails, it may be because the user needs
        // to login or grant consent to one or more of the requested scopes
        
        /*if (this.isInteractionRequired(err)) {
          var interactiveResult = await userAgentApplication.acquireTokenPopup(
            {
              scopes: scopes
            }
          );

          return interactiveResult.accessToken;
        } else {
          throw err;
        }*/
      }
}

//---------------------------------------------------------------------------------
export const getUserProfile = async () => {
    try {
      var accessToken = await getAccessToken(config.scopes);

      if (accessToken) {

        
        // Get the user's profile from Graph
        const {user, userExt} = await getUserDetails(accessToken);
        store.dispatch(setUser(user))
        var photo = await getUserPhoto(accessToken)
        store.dispatch(setUserPhoto(photo))

        //console.log(userExt.homePage)
        //await Login(accessToken)
        /*this.setState({
          isAuthenticated: true,
          user: {
            displayName: user.displayName,
            email: user.mail || user.userPrincipalName
          },
          error: null
        });
        */
      }
    } catch (err) {
      /*this.setState({
        isAuthenticated: false,
        user: {},
        error: this.normalizeError(err)
      });*/
    }
  };
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
