import axios from "axios"

//const BASE_URL = 'http://localhost:4000/v1'
const BASE_URL = 'https://134.122.65.239:4000/v1'

export const getMusteriler = async (filter) => {
    return new Promise((resolve, reject) => {
        axios.get(`${BASE_URL}/musteri`,
        {
            params: { filter : filter },
            headers: {authorization: window.localStorage.getItem('experto_access_token')}
        }).then(resp => resolve(resp.data))
    })
}

export const getMusteri = async (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${BASE_URL}/musteri/${id}`,
        {
            headers: {authorization: window.localStorage.getItem('experto_access_token')}
        }).then(resp => resolve(resp.data))
    })
}

export const updateMusteri = async (id, data) => {
    return new Promise((resolve, reject) => {
        axios.patch(`${BASE_URL}/musteri/${id}`,
        data,
        {
            headers: {authorization: window.localStorage.getItem('experto_access_token')}
        }).then(resp => resolve(resp.data))
    })
}

export const getMusteriKisiler = async (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${BASE_URL}/musteri/${id}/kisiler`,
        {
            headers: {authorization: window.localStorage.getItem('experto_access_token')}
        }).then(resp => resolve(resp.data))
    })
}

export const getSektorler = async () => {
    return new Promise((resolve, reject) => {
        axios.get(`${BASE_URL}/sektor`,
        {
            headers: {authorization: window.localStorage.getItem('experto_access_token')}
        }).then(resp => resolve(resp.data))
    })
}

export const getUrunler = async (filter) => {
    return new Promise((resolve, reject) => {
        axios.get(`${BASE_URL}/urun`,
        {
            params: {filter: filter},
            headers: {authorization: window.localStorage.getItem('experto_access_token')}
        }).then(resp => resolve(resp.data))
    })
}
