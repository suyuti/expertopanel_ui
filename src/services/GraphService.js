//import { BatchRequestContent, BatchResponseContent } from '@microsoft/microsoft-graph-client';

var graph = require('@microsoft/microsoft-graph-client');

//-----------------------------------------------------------------------------

function getAuthenticatedClient(accessToken) {
  // Initialize Graph client
  const client = graph.Client.init({
    // Use the provided access token to authenticate
    // requests
    authProvider: done => {
      done(null, accessToken);
    }
  });

  return client;
}

//-----------------------------------------------------------------------------

export async function getUserDetails(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  const userExt = await client.api('/me/extensions/expertoSettings').get();
  const user    = await client.api('/me').get();
//  debugger
  return {user, userExt};
}

//-----------------------------------------------------------------------------

export async function getUserPhoto(accessToken) {
  return _getUserPhoto(accessToken, 'me')
  /*  const client = getAuthenticatedClient(accessToken);

  const photo = await client.api('/me/photo/$value').get();
  return window.URL.createObjectURL(photo)
  */
}

//-----------------------------------------------------------------------------

export async function getEvents(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  //const events = await 
  return client
    .api('/me/events')
    .select('subject,organizer,start,end')
    .orderby('createdDateTime DESC')
    .get();

  //return events;
}

//-----------------------------------------------------------------------------

export async function _getUserPhoto(accessToken, userId) {
  const client = getAuthenticatedClient(accessToken);
  try {
    if (userId === 'me') {
      const photo = await client.api(`/me/photo/$value`).get();
      return window.URL.createObjectURL(photo)
    }
    else {
      const photo = await client.api(`/users/${userId}/photo/$value`).get();
      return window.URL.createObjectURL(photo)
      }
  }
  catch(e) {
  }
}

export async function getMails(accessToken, musteriDomain) {
  const client = getAuthenticatedClient(accessToken);

  var req = client.api('/me/messages')
  if (musteriDomain) {
    req.filter(`contains(from/emailAddress/address, '${musteriDomain}')`)
  }
  //req.orderby('sentDateTime DESC')
  const mails = await req.get()

/*  const events = await client
    .api('/me/messages')
    //.select('subject,organizer,start,end')
    //.orderby('createdDateTime DESC')
    .get();
*/
  return mails;
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

export async function getAllMyGroups(accessToken) {
  const client = getAuthenticatedClient(accessToken);
  var req = client.api('/me/memberOf')
  const groups = await req.get()
  return groups.value;
}

export async function getGroupMembers(accessToken, groupId) {
  const client = getAuthenticatedClient(accessToken);
  var req = client.api(`/groups/${groupId}/members`)
  var members = await req.get()
  for (var i = 0; i < members.value.length; ++i) {
    const photo = await _getUserPhoto(accessToken, members.value[i].id)
    members.value[i].photo = photo
  }
  return members.value;
}


var cacheGroups = {}
var cacheUsers = []



export async function getGroupMembersByName(accessToken, groupName) {
  const client = getAuthenticatedClient(accessToken);





  //var group = {}
  //if (cacheGroups[`${groupName}`]) {
  //  group = cacheGroups[`${groupName}`]
  //}
  //else {
    var group = await getGroupByName(accessToken, groupName)
  //  cacheGroups[`${groupName}`] = group
  //}

//  if (cacheUsers.length == 0) {
    cacheUsers = await getGroupMembers(accessToken, group.id)
//  }
  return cacheUsers;
}

export async function getGroupByName(accessToken, groupName) {
  const client = getAuthenticatedClient(accessToken);
  var group = await client.api(`/groups`).select('displayName, id').filter(`displayName eq '${groupName}'`).get()
  return group.value[0]
}



//-----------------------------------------------------------------------------

export async function getAllPersonsOnMyGroups(accessToken) {
  const client = getAuthenticatedClient(accessToken);
  var members = []
  const groups = await getAllMyGroups(accessToken)
  for (var i = 0; i < groups.length; ++i) {
    const _members = await getGroupMembers(accessToken, groups[i].id)
    members = [...members, ..._members]
  }
  return members
}

//-----------------------------------------------------------------------------

export async function getUser(accessToken, uid) {
  const user = cacheUsers.find(u => u.id === uid)
  if (user) {
    return user
  }

  const client = getAuthenticatedClient(accessToken)
  var _user = await client.api(`/users/${uid}`).select('displayName, jobTitle, id').get()
  var userPhoto = await _getUserPhoto(accessToken, uid)
  _user.photo = userPhoto
  
  cacheUsers.push(_user)

  return user
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


export async function getUserList(accessToken, groupName) {
    try {
    debugger
    const client  = getAuthenticatedClient(accessToken)
    const group   = await getGroupByName(accessToken, groupName)
    const members = await getGroupMembers(accessToken, group.id)
    var requests = {
      'requests' : []
    }

    for (var i = 1; i <= members.length; ++i) {
      requests.requests.push({
        'id': members[i-1].id,
        'method': 'GET',
        'url': `/users/${members[i-1].id}/photo/$value`
      })
      if ((i % 20) == 0) {
        debugger
        const photos = await client.api('/$batch').post(requests)
        photos.responses.map(p => {
          if (p.status === 200) {
            var m = members.find(m => m.id === p.id)
            m.photo = window.URL.createObjectURL(p.body)
          }
        })
        requests.requests = []
      }
    }
/*
    members.map(m => requests.requests.push({
      'id': m.id,
      'method': 'GET',
      'url': `/users/${m.id}/photo/$value`
    }))

    const photos = await client.api('/$batch').post(requests)
    
    members.map(m => {
      const photoResponse = photos.responses.find(r => r.id == m.id)
      if (photoResponse.status == 200) {
        m.photo = window.URL.createObjectURL(photoResponse.body)
      }
    })
    */
  }
  catch (e) {
    debugger
    console.log(e)
  }
}

var cache = {
  groups: {},
  users: null,
  domains: {} // mailler icin
}

export async function useGroup(accessToken, name) {
  if (cache.groups[name]) {
    console.log('cacheden')
    return cache.groups[name]
  }

  var group = await getGroupByName(accessToken, name)
  var members = await getGroupMembers(accessToken, group.id)
  cache.groups[name] = {
    members:members
  }

  console.log(cache.groups[name])
  return cache.groups[name]
}

//---------------------------------------------------------------------

export async function getUsers(accessToken) {
  if (cache.users) {
    return cache.users
  }
  const client = getAuthenticatedClient(accessToken)
  var users = await client.api(`/users`)
  .select('displayName, jobTitle, id, userPrincipalName')
  .get()
  cache.users = users.value

  return cache.users
}

//---------------------------------------------------------------------

export async function useEmails(accessToken, domain) {
  var members = await getUsers(accessToken)
  debugger
  const client = getAuthenticatedClient(accessToken);

  for (var i = 0; i < members.length; ++i) {
    try {
      const m = members[i]
      console.log(m)
      var req = client.api(`/users/${m.id}/mailfolders`)
      if (domain) {
        //req.filter(`contains(from/emailAddress/address, '${domain}')`)
      }
      const mails = await req.get()
      debugger
      console.log(mails)
      }
    catch(e) {
      console.log(e)
    }
  }
}