var graph = require('@microsoft/microsoft-graph-client');

const getAuthenticatedClient = (accessToken) => {
    const client = graph.Client.init({
      authProvider: done => {
        done(null, accessToken);
      }
    });
    return client;
}

var client = client || getAuthenticatedClient(window.localStorage.getItem('token'))
  
//------------------------------------------------------------------
// Grup uyelerini listeler

export const getGrupUyeleri = async (grupId) => {
    var users = await client.api(`/me`)
    .select('displayName, jobTitle, id, userPrincipalName')
    .get()
    return users
}

//------------------------------------------------------------------
// Kullanicinin uyesi oldugu gruplari listeler

export const getUserGroups = async (userId) => {

}


