import { UserAgentApplication } from 'msal';
import { config } from '../Config';

var graph = require('@microsoft/microsoft-graph-client');

export const userAgentApplication = new UserAgentApplication({
  auth: {
    clientId: config.appId,
    redirectUri: config.redirectUri
  },
  cache: {
    cacheLocation: 'sessionStorage',
    storeAuthStateInCookie: true
  }
});

const getAuthenticatedClient = accessToken => {
  const client = graph.Client.init({
    authProvider: done => {
      done(null, accessToken);
    }
  });
  return client;
};

export var client = getAuthenticatedClient(
  window.localStorage.getItem('token')
);

//-----------------------------------------------------------------------------------------------------------
export const silentLogin = async redirect => {
  try {
    let promise = new Promise((resolve, reject) => {
      const uid = window.localStorage.getItem('uniqueId');
      if (uid) {
        getAccessToken(config.scopes, uid).then(respToken => {
          window.localStorage.setItem('token', respToken); // Graph token localstorage da tutulur
          client = getAuthenticatedClient(respToken);
          getUser(uid).then(me => {
            getUserPhoto(me.id)
              .then(photo => {
                resolve({
                  uniqueId: uid,
                  token: respToken,
                  me: me,
                  mePhoto: photo
                });
              })
              .catch(r => {
                resolve({
                  uniqueId: uid,
                  token: respToken,
                  me: me,
                  mePhoto: null
                });
              });
          });
        });
      } else {
        debugger
        userAgentApplication
          .loginPopup({
            scopes: config.scopes,
            prompt: 'select_account'
          })
          .then(resp => {
            debugger
            getAccessToken(config.scopes, resp.uniqueId).then(respToken => {
              window.localStorage.setItem('token', respToken); // Graph token localstorage da tutulur
              client = getAuthenticatedClient(respToken);
              getUser(resp.uniqueId).then(me => {
                getUserPhoto(me.id)
                  .then(photo => {
                    resolve({
                      uniqueId: resp.uniqueId,
                      token: respToken,
                      me: me,
                      mePhoto: photo
                    });
                  })
                  .catch(r => {
                    resolve({
                      uniqueId: resp.uniqueId,
                      token: respToken,
                      me: me,
                      mePhoto: null
                    });
                  });
              });
            });
          });
      }
    });
    return promise;
  } catch (err) {
    console.log(err);
  }
};

export const login = async redirect => {
  try {
    let promise = new Promise((resolve, reject) => {
      userAgentApplication
        .loginPopup({
          scopes: config.scopes,
          prompt: 'select_account'
        })
        .then(resp => {
          getAccessToken(config.scopes, resp.uniqueId).then(respToken => {
            window.localStorage.setItem('token', respToken); // Graph token localstorage da tutulur
            client = getAuthenticatedClient(respToken);
            getUser(resp.uniqueId).then(me => {
              getUserPhoto(me.id)
                .then(photo => {
                  resolve({
                    uniqueId: resp.uniqueId,
                    token: respToken,
                    me: me,
                    mePhoto: photo
                  });
                })
                .catch(r => {
                  resolve({
                    uniqueId: resp.uniqueId,
                    token: respToken,
                    me: me,
                    mePhoto: null
                  });
                });
            });
          });
        });
    });
    return promise;

    //        .then(authResponse => {
    //          return getAccessToken(config.scopes)
    /*.then(accessToken => 
            {
              
              //getUserProfile().then(resp => console.log(resp))
            
            //Login(accessToken, authResponse.uniqueId).then(resp => {
              //window.localStorage.setItem('experto_access_token', resp.data.expertoToken)
              //window.localStorage.setItem('token', accessToken)
              //redirect(resp.data.redirectUrl)
            })*/
    //            }
    //          )
  } catch (err) {
    console.log(err);
  }
};

//-----------------------------------------------------------------------------------------------------------

export const logout = async grupId => {
  var users = await client
    .api(`/me`)
    .select('displayName, jobTitle, id, userPrincipalName')
    .get();
  return users;
};

//-----------------------------------------------------------------------------------------------------------
export const getAccessToken = async scopes => {
  try {
    var silentResult = await userAgentApplication.acquireTokenSilent({
      scopes: scopes
    });

    return silentResult.accessToken;
  } catch (err) {}
};

//-----------------------------------------------------------------------------------------------------------

export const getMe = async () => {
  const user = await client.api('/me').get();
  return user;
};

//-----------------------------------------------------------------------------------------------------------

export const getUsers = async () => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users`)
      .get()
      .then(resp => resolve(resp.value));
  });
};

export const getUser = async userId => {
  const user = await client.api(`/users/${userId}`).get();
  return user;
};

//-----------------------------------------------------------------------------------------------------------

export const getUserPhoto = async userId => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${userId}/photo/$value`)
      .get()
      .then(photo => resolve(window.URL.createObjectURL(photo)))
      .catch(e => resolve(null));
  });
};

export const getUserPhoto2 = async user => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${user.id}/photo/$value`)
      .get()
      .then(photo => {
        user.photo = window.URL.createObjectURL(photo);
        resolve(user);
      })
      .catch(e => resolve(null));
  });
};

//-----------------------------------------------------------------------------------------------------------

export const getGroupByName = async groupName => {
  return new Promise((resolve, reject) => {
    client
      .api(`/groups`)
      .select('displayName, id')
      .filter(`displayName eq '${groupName}'`)
      .get()
      .then(group => {
        if (group.value.length > 0) {
          getGroupMembers(group.value[0].id).then(m => resolve(m));
        } else {
          resolve(null);
        }
      });
  });
};

//-----------------------------------------------------------------------------------------------------------

export const getGroupMembers = async groupId => {
  var members = await client.api(`/groups/${groupId}/members`).get();
  /*for (var i = 0; i < members.value.length; ++i) {
      const photo = await getUserPhoto(members.value[i].id);
      members.value[i].photo = photo
    }*/
  return members.value;
};

//-----------------------------------------------------------------------------------------------------------
export const getUserGorevler = async userId => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${userId}/planner/tasks`)
      .get()
      .then(resp => resolve(resp.value))
      .catch(e => resolve(null));
  });
};

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
