import * as ActionsMe from '../actions/me.actions';
import * as ActionsMusteri from '../actions/musteriler.actions';
import * as ActionsUrun from '../actions/urunler.actions';

import '../services/AuthService';

// Auth

export const IS_AUTH = 'IS_AUTHENTICATED';
export const SET_USER = 'SET_USER';
export const SET_USER_PHOTO = 'SET_USER_PHOTO';
export const SET_MUSTERI_REQ = 'SET_MUSTERI_REQ';
export const SET_MUSTERI_SUC = 'SET_MUSTERI_SUC';
export const SET_MUSTERI = 'SET_MUSTERI';

export const setAuthenticated = auth => ({
  type: IS_AUTH,
  auth
});

export const setUser = user => ({
  type: SET_USER,
  user
});

export const setUserPhoto = photo => ({
  type: SET_USER_PHOTO,
  photo
});

export const setMusteri = musteri => ({
  type: SET_MUSTERI,
  musteri
});

// Sidebar

export const SET_SIDEBAR_SHADOW = 'THEME_OPTIONS/SET_SIDEBAR_SHADOW';
export const SET_SIDEBAR_TOGGLE_MOBILE =
  'THEME_OPTIONS/SET_SIDEBAR_TOGGLE_MOBILE';
export const SET_SIDEBAR_FIXED = 'THEME_OPTIONS/SET_SIDEBAR_FIXED';
export const SET_SIDEBAR_FOOTER = 'THEME_OPTIONS/SET_SIDEBAR_FOOTER';
export const SET_SIDEBAR_TOGGLE = 'THEME_OPTIONS/SET_SIDEBAR_TOGGLE';
export const SET_SIDEBAR_USERBOX = 'THEME_OPTIONS/SET_SIDEBAR_USERBOX';
export const SET_SIDEBAR_HOVER = 'THEME_OPTIONS/SET_SIDEBAR_HOVER';

export const setSidebarShadow = sidebarShadow => ({
  type: SET_SIDEBAR_SHADOW,
  sidebarShadow
});
export const setSidebarFixed = sidebarFixed => ({
  type: SET_SIDEBAR_FIXED,
  sidebarFixed
});
export const setSidebarToggleMobile = sidebarToggleMobile => ({
  type: SET_SIDEBAR_TOGGLE_MOBILE,
  sidebarToggleMobile
});
export const setSidebarFooter = sidebarFooter => ({
  type: SET_SIDEBAR_FOOTER,
  sidebarFooter
});
export const setSidebarToggle = sidebarToggle => ({
  type: SET_SIDEBAR_TOGGLE,
  sidebarToggle
});
export const setSidebarHover = sidebarHover => ({
  type: SET_SIDEBAR_HOVER,
  sidebarHover
});
export const setSidebarUserbox = sidebarUserbox => ({
  type: SET_SIDEBAR_USERBOX,
  sidebarUserbox
});
// Header

export const SET_HEADER_FIXED = 'THEME_OPTIONS/SET_HEADER_FIXED';
export const SET_HEADER_SHADOW = 'THEME_OPTIONS/SET_HEADER_SHADOW';
export const SET_HEADER_SEARCH_HOVER = 'THEME_OPTIONS/SET_HEADER_SEARCH_HOVER';

export const setHeaderFixed = headerFixed => ({
  type: SET_HEADER_FIXED,
  headerFixed
});
export const setHeaderShadow = headerShadow => ({
  type: SET_HEADER_SHADOW,
  headerShadow
});
export const setHeaderSearchHover = headerSearchHover => ({
  type: SET_HEADER_SEARCH_HOVER,
  headerSearchHover
});

// Main content

export const SET_CONTENT_BACKGROUND = 'THEME_OPTIONS/SET_CONTENT_BACKGROUND';
export const SET_THEME_CONFIGURATOR_TOGGLE =
  'THEME_OPTIONS/SET_THEME_CONFIGURATOR_TOGGLE';

export const setContentBackground = contentBackground => ({
  type: SET_CONTENT_BACKGROUND,
  contentBackground
});
export const setThemeConfiguratorToggle = themeConfiguratorToggle => ({
  type: SET_THEME_CONFIGURATOR_TOGGLE,
  themeConfiguratorToggle
});
// Footer

export const SET_FOOTER_FIXED = 'THEME_OPTIONS/SET_FOOTER_FIXED';
export const SET_FOOTER_SHADOW = 'THEME_OPTIONS/SET_FOOTER_SHADOW';
export const setFooterFixed = footerFixed => ({
  type: SET_FOOTER_FIXED,
  footerFixed
});
export const setFooterShadow = footerShadow => ({
  type: SET_FOOTER_SHADOW,
  footerShadow
});

// Page title

export const SET_PAGE_TITLE_STYLE = 'THEME_OPTIONS/SET_PAGE_TITLE_STYLE';
export const SET_PAGE_TITLE_BACKGROUND =
  'THEME_OPTIONS/SET_PAGE_TITLE_BACKGROUND';
export const SET_PAGE_TITLE_SHADOW = 'THEME_OPTIONS/SET_PAGE_TITLE_SHADOW';
export const SET_PAGE_TITLE_BREADCRUMB =
  'THEME_OPTIONS/SET_PAGE_TITLE_BREADCRUMB';
export const SET_PAGE_TITLE_ICON_BOX = 'THEME_OPTIONS/SET_PAGE_TITLE_ICON_BOX';
export const SET_PAGE_TITLE_DESCRIPTION =
  'THEME_OPTIONS/SET_PAGE_TITLE_DESCRIPTION';

export const setPageTitleStyle = pageTitleStyle => ({
  type: SET_PAGE_TITLE_STYLE,
  pageTitleStyle
});
export const setPageTitleBackground = pageTitleBackground => ({
  type: SET_PAGE_TITLE_BACKGROUND,
  pageTitleBackground
});
export const setPageTitleShadow = pageTitleShadow => ({
  type: SET_PAGE_TITLE_SHADOW,
  pageTitleShadow
});
export const setPageTitleBreadcrumb = pageTitleBreadcrumb => ({
  type: SET_PAGE_TITLE_BREADCRUMB,
  pageTitleBreadcrumb
});
export const setPageTitleIconBox = pageTitleIconBox => ({
  type: SET_PAGE_TITLE_ICON_BOX,
  pageTitleIconBox
});
export const setPageTitleDescription = pageTitleDescription => ({
  type: SET_PAGE_TITLE_DESCRIPTION,
  pageTitleDescription
});
export default function reducer(
  state = {
    // Auth
    isAuthenticated: false,
    user: null,
    userPhoto: '',
    musteri: null,

    token: null, // token
    me: null, // kullanici
    mePhoto: null, // kullanici resmi
    myGroups: [],
    groupMembers: {},
    musteriKisiler: [], // secili musterinin kisiler listesi
    sektorler: [],
    urunler: null,
    users: null, // sistemdeki tum kullanicilar/ Microsoft Graph
    musteriler: null, // tum musteri listesi
    gorevlerim: null,
    yetkiler: null,

    // Sidebar

    sidebarShadow: false,
    sidebarFixed: true,
    sidebarToggleMobile: false,
    sidebarFooter: true,
    sidebarUserbox: true,
    sidebarToggle: false,
    sidebarHover: false,
    // Header

    headerFixed: true,
    headerShadow: true,
    headerSearchHover: false,

    // Main content

    contentBackground: '',
    themeConfiguratorToggle: false,
    // Footer

    footerFixed: false,
    footerShadow: false,
    // Page title

    pageTitleStyle: '',
    pageTitleBackground: '',
    pageTitleShadow: false,
    pageTitleBreadcrumb: false,
    pageTitleIconBox: true,
    pageTitleDescription: true
  },
  action
) {
  switch (action.type) {
    // Sidebar

    case SET_SIDEBAR_SHADOW:
      return {
        ...state,
        sidebarShadow: action.sidebarShadow
      };
    case SET_SIDEBAR_FIXED:
      return {
        ...state,
        sidebarFixed: action.sidebarFixed
      };
    case SET_SIDEBAR_TOGGLE_MOBILE:
      return {
        ...state,
        sidebarToggleMobile: action.sidebarToggleMobile
      };
    case SET_SIDEBAR_FOOTER:
      return {
        ...state,
        sidebarFooter: action.sidebarFooter
      };
    case SET_SIDEBAR_TOGGLE:
      return {
        ...state,
        sidebarToggle: action.sidebarToggle
      };
    case SET_SIDEBAR_HOVER:
      return {
        ...state,
        sidebarHover: action.sidebarHover
      };
    case SET_SIDEBAR_USERBOX:
      return {
        ...state,
        sidebarUserbox: action.sidebarUserbox
      };
    // Header

    case SET_HEADER_FIXED:
      return {
        ...state,
        headerFixed: action.headerFixed
      };
    case SET_HEADER_SHADOW:
      return {
        ...state,
        headerShadow: action.headerShadow
      };
    case SET_HEADER_SEARCH_HOVER:
      return {
        ...state,
        headerSearchHover: action.headerSearchHover
      };

    // Main content

    case SET_CONTENT_BACKGROUND:
      return {
        ...state,
        contentBackground: action.contentBackground
      };
    case SET_THEME_CONFIGURATOR_TOGGLE:
      return {
        ...state,
        themeConfiguratorToggle: action.themeConfiguratorToggle
      };
    // Footer

    case SET_FOOTER_FIXED:
      return {
        ...state,
        footerFixed: action.footerFixed
      };
    case SET_FOOTER_SHADOW:
      return {
        ...state,
        footerShadow: action.footerShadow
      };

    // Page title

    case SET_PAGE_TITLE_STYLE:
      return {
        ...state,
        pageTitleStyle: action.pageTitleStyle
      };
    case SET_PAGE_TITLE_BACKGROUND:
      return {
        ...state,
        pageTitleBackground: action.pageTitleBackground
      };
    case SET_PAGE_TITLE_SHADOW:
      return {
        ...state,
        pageTitleShadow: action.pageTitleShadow
      };
    case SET_PAGE_TITLE_BREADCRUMB:
      return {
        ...state,
        pageTitleBreadcrumb: action.pageTitleBreadcrumb
      };
    case SET_PAGE_TITLE_ICON_BOX:
      return {
        ...state,
        pageTitleIconBox: action.pageTitleIconBox
      };
    case SET_PAGE_TITLE_DESCRIPTION:
      return {
        ...state,
        pageTitleDescription: action.pageTitleDescription
      };
    case IS_AUTH:
      return {
        ...state,
        isAuthenticated: action.auth
      };
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthenticated: true
      };
    case SET_USER_PHOTO:
      return {
        ...state,
        userPhoto: action.photo
      };

    case SET_MUSTERI:
      return {
        ...state,
        musteri: action.musteri
      };

    case ActionsMe.LOGIN_ERR:
    case ActionsMe.LOGIN_REQ: {
      return {
        ...state,
        isAuthenticated: false,
        token: null,
        me: null,
        mePhoto: null,
        yetkiler: null
      };
    }
    case ActionsMe.LOGIN_SUC: {
      return {
        ...state,
        isAuthenticated: true,
        token: action.token,
        me: action.me,
        mePhoto: action.mePhoto,
        yetkiler: action.yetkiler
      };
    }
    case ActionsMusteri.GET_MUSTERI_KISILER_ERR:
    case ActionsMusteri.GET_MUSTERI_KISILER_REQ: {
      return {
        ...state,
        musteriKisiler: []
      };
    }
    case ActionsMusteri.GET_MUSTERI_KISILER_SUC: {
      return {
        ...state,
        musteriKisiler: action.musteriKisiler
      };
    }
    case ActionsMusteri.GET_MUSTERI_ERR:
    case ActionsMusteri.GET_MUSTERI_REQ: {
      return {
        ...state,
        musteri: null
      };
    }
    case ActionsMusteri.GET_MUSTERI_SUC: {
      return {
        ...state,
        musteri: action.musteri
      };
    }
    case ActionsMusteri.SAVE_MUSTERI_SUC: {
      return {
        ...state,
        musteri: action.musteri
      };
    }

    case ActionsMusteri.GET_SEKTORLER_REQ: {
      return {
        ...state,
        sektorler: []
      };
    }
    case ActionsMusteri.GET_SEKTORLER_SUC: {
      return {
        ...state,
        sektorler: action.sektorler
      };
    }
    case ActionsUrun.GET_URUNLER_REQ:
    case ActionsUrun.GET_URUNLER_ERR: {
      return {
        ...state,
        urunler: null
      };
    }
    case ActionsUrun.GET_URUNLER_SUC: {
      return {
        ...state,
        urunler: action.urunler
      };
    }
    case ActionsMe.GET_USERS_ERR:
    case ActionsMe.GET_USERS_REQ: {
      return {
        ...state,
        users: null
      };
    }
    case ActionsMe.GET_USERS_SUC: {
      return {
        ...state,
        users: action.users
      };
    }
    case ActionsMusteri.GET_MUSTERILER_REQ:
    case ActionsMusteri.GET_MUSTERILER_ERR: {
      return {
        ...state,
        musteriler: null
      };
    }
    case ActionsMusteri.GET_MUSTERILER_SUC: {
      return {
        ...state,
        musteriler: action.musteriler
      };
    }
    case ActionsMe.GET_GOREVLERIM_REQ:
    case ActionsMe.GET_GOREVLERIM_ERR: {
      return {
        ...state,
        gorevlerim: null
      };
    }
    case ActionsMe.GET_GOREVLERIM_SUC: {
      return {
        ...state,
        gorevlerim: action.gorevlerim
      };
    }

    default:
      break;
  }
  return state;
}
