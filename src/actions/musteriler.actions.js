import axios from "axios";
import {getMusteri as ApiGetMusteri} from '../services/expertoService'
import {getMusteriler as ApiGetMusteriler} from '../services/expertoService'
import {getMusteriKisiler as ApiGetMusteriKisiler} from '../services/expertoService'
import {updateMusteri as ApiUpdateMusteri} from '../services/expertoService'
import {getSektorler as ApiGetSektorler} from '../services/expertoService'

export const GET_MUSTERILER_REQ = 'GET MUSTERILER REQ'
export const GET_MUSTERILER_ERR = 'GET MUSTERILER ERR'
export const GET_MUSTERILER_SUC = 'GET MUSTERILER SUC'

export const GET_MUSTERI_REQ = 'GET MUSTERI REQ'
export const GET_MUSTERI_ERR = 'GET MUSTERI ERR'
export const GET_MUSTERI_SUC = 'GET MUSTERI SUC'

export const SAVE_MUSTERI_REQ = 'SAVE MUSTERI REQ'
export const SAVE_MUSTERI_ERR = 'SAVE MUSTERI ERR'
export const SAVE_MUSTERI_SUC = 'SAVE MUSTERI SUC'

export const GET_MUSTERI_KISILER_REQ = 'GET MUSTERILER KISILER REQ'
export const GET_MUSTERI_KISILER_SUC = 'GET MUSTERILER KISILER SUC'
export const GET_MUSTERI_KISILER_ERR = 'GET MUSTERILER KISILER ERR'

export const SET_MUSTERI = 'SET MUSTERI'

export const GET_SEKTORLER_REQ = 'GET SEKTORLER REQ'
export const GET_SEKTORLER_SUC = 'GET SEKTORLER SUC'



export function getMusteriler(params) {
    const req = ApiGetMusteriler(params)
    return (dispatch) => {
        dispatch({type: GET_MUSTERILER_REQ})
        req.then(resp => {
            dispatch({
                type: GET_MUSTERILER_SUC,
                musteriler: resp
            })
        })
    }
}

export function getMusteri(mId) {
    const req = ApiGetMusteri(mId)
    return (dispatch) => {
        dispatch({type: GET_MUSTERI_REQ})
        req.then(resp => {
            dispatch({
                type: GET_MUSTERI_SUC,
                musteri: resp
            })
        })
    }
}

export function getMusteriKisiler(mId) {
    const req = ApiGetMusteriKisiler(mId) //axios.get('')
    return (dispatch) => {
        dispatch({type: GET_MUSTERI_KISILER_REQ})
        req.then(resp => {
            dispatch({
                type: GET_MUSTERI_KISILER_SUC,
                musteriKisiler: resp
            })
        })
    }
}

export function saveMusteri(musteri) {
    const req = ApiUpdateMusteri(musteri._id, musteri)
    return (dispatch) => {
        dispatch({type: SAVE_MUSTERI_REQ})
        req.then(resp => {
            dispatch({
                type: SAVE_MUSTERI_SUC,
                musteri: resp
            })
        })
    }
}

export function getSektorler() {
    const req = ApiGetSektorler()
    return (dispatch) => {
        dispatch({type: GET_SEKTORLER_REQ})
        req.then(resp => {
            dispatch({
                type: GET_SEKTORLER_SUC,
                sektorler: resp
            })
        })
    }
}
