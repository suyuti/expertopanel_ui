import axios from 'axios';
import { login as LoginGraph, getUserPhoto } from '../services/graph_Service';
import { login as LoginExperto } from '../services/ApiService';
import { getUsers as GraphGetUsers } from '../services/graph_Service'
import { getUserPhoto2 as GraphGetUserPhoto2 } from '../services/graph_Service'
import { getUserGorevler as GraphGetUserGorevler } from '../services/graph_Service'

export const LOGIN_REQ = 'LOGIN REQ';
export const LOGIN_SUC = 'LOGIN SUC';
export const LOGIN_ERR = 'LOGIN ERR';

export function login(redirect) {
  const req = LoginGraph(redirect);
  return dispatch => {
    dispatch({ type: LOGIN_REQ });
    req.then(resp => {
      if (resp) {
        // 1. Microsoft'a login oldu (token, me, mePhoto)
        LoginExperto(resp.token, resp.uniqueId).then(r => {
          // 2. Experto'ya login (redirect Url)
          window.localStorage.setItem(
            'experto_access_token',
            r.data.expertoToken
          );
          //window.localStorage.setItem('token', resp.token); // Graph token localstorage da tutulur

          dispatch(getUsers())

          //console.log(resp)
          dispatch({
            type: 'LOGIN SUC',
            token: resp.token,
            me: resp.me,
            mePhoto: resp.mePhoto,
            yetkiler: r.data.yetkiler 
          });

          redirect(r.data.redirectUrl);
        });
      }
    });
  };
}

//----------------------------------------------------------

export const GET_USERS_REQ = 'GET USERS REQ'
export const GET_USERS_ERR = 'GET USERS ERR'
export const GET_USERS_SUC = 'GET USERS SUC'

export async function _getUsers() {
  try {
    const users = await GraphGetUsers()
    if (users) {
      for(var i = 0; i < users.length; ++i) {
        //const photo = await getUserPhoto(users[i].id)
        //users[i].photo = photo
      }
    }
    return dispatch => {
      dispatch({type: GET_USERS_SUC, users: users})
    }
  }
  catch(e) {
    debugger
  }
}

export function getUsers() {
  const req = GraphGetUsers()
  return dispatch => {
    dispatch({type: GET_USERS_REQ})
    req.then(users => {
      for (var i = 0; i < users.length; ++i) {
        GraphGetUserPhoto2(users[i])
      }
      dispatch({type: GET_USERS_SUC, users: users})
    })
  }
}

//------------------------------------------------------------

export const GET_GOREVLERIM_REQ = 'GET_GOREVLERIM_REQ'
export const GET_GOREVLERIM_ERR = 'GET_GOREVLERIM_ERR'
export const GET_GOREVLERIM_SUC = 'GET_GOREVLERIM_SUC'

export function getGorevlerim(meId) {
  const req = GraphGetUserGorevler(meId)
  return dispatch => {
    dispatch({type: GET_GOREVLERIM_REQ})
    req.then(gorevlerim => {
      dispatch({type: GET_GOREVLERIM_SUC, gorevlerim: gorevlerim})
    })
  }
}

