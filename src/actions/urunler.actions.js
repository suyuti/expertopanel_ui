import {getUrunler as ApiGetUrunler} from 'services/expertoService'

export const GET_URUNLER_REQ = 'GET URUNLER REQ'
export const GET_URUNLER_ERR = 'GET URUNLER ERR'
export const GET_URUNLER_SUC = 'GET URUNLER SUC'



export function getUrunler(filter) {
    const req = ApiGetUrunler(filter)
    return (dispatch) => {
        dispatch({type: GET_URUNLER_REQ})
        req.then(resp => {
            dispatch({
                type: GET_URUNLER_SUC,
                urunler: resp
            })
        })
    }
}
