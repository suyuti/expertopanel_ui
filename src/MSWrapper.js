import React, { Component, Fragment, Children } from 'react';
import { UserAgentApplication } from 'msal';
import { config } from './Config';
import { connect } from 'react-redux';
import {
    setAuthenticated
  } from './reducers/ThemeOptions';
  
class MSWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
        error: null,
        isAuthenticated: false,
        user: {}
      };
    this.userAgentApplication = new UserAgentApplication({
      auth: {
        clientId: config.appId,
        redirectUri: config.redirectUri
      },
      cache: {
        cacheLocation: 'sessionStorage',
        storeAuthStateInCookie: true
      }
    });
  }

  componentDidMount() {
    // If MSAL already has an account, the user
    // is already logged in
    var account = this.userAgentApplication.getAccount();
    if (account) {
        setAuthenticated(true)
      // Enhance user object with data from Graph
      //this.getUserProfile();
    }
    else {
        setAuthenticated(false)
    }
  }

  render() {
    return (
      <Fragment
        error={this.state.error}
        isAuthenticated={this.state.isAuthenticated}
        user={this.state.user}
        login={() => this.login()}
        logout={() => this.logout()}
        getAccessToken={scopes => this.getAccessToken(scopes)}
        setError={(message, debug) => this.setErrorMessage(message, debug)}
        {...this.props}
        {...this.state}
      >
          {this.props.children}
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  });
  
const mapDispatchToProps = dispatch => ({
    setAuhtenticated: auth => dispatch(setAuthenticated(auth)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MSWrapper);
