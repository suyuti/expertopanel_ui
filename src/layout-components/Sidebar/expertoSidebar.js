import BarChartIcon from '@material-ui/icons/BarChart';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ChatIcon from '@material-ui/icons/ChatOutlined';
import CodeIcon from '@material-ui/icons/Code';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import FolderIcon from '@material-ui/icons/FolderOutlined';
import DashboardTwoToneIcon from '@material-ui/icons/DashboardTwoTone';
import GradeTwoTone from '@material-ui/icons/GradeTwoTone';
import ListAltIcon from '@material-ui/icons/ListAlt';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import MailIcon from '@material-ui/icons/MailOutlined';
import PresentToAllIcon from '@material-ui/icons/PresentToAll';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import PersonIcon from '@material-ui/icons/PersonOutlined';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';
import SettingsIcon from '@material-ui/icons/SettingsOutlined';
import ViewModuleIcon from '@material-ui/icons/ViewModule';

var iconsMap = {
  BarChartIcon: BarChartIcon,
  CalendarTodayIcon: CalendarTodayIcon,
  ChatIcon: ChatIcon,
  CodeIcon: CodeIcon,
  DashboardIcon: DashboardIcon,
  ErrorIcon: ErrorIcon,
  FolderIcon: FolderIcon,
  DashboardTwoToneIcon: DashboardTwoToneIcon,
  GradeTwoTone: GradeTwoTone,
  ListAltIcon: ListAltIcon,
  LockOpenIcon: LockOpenIcon,
  MailIcon: MailIcon,
  PresentToAllIcon: PresentToAllIcon,
  PeopleIcon: PeopleIcon,
  PersonIcon: PersonIcon,
  ReceiptIcon: ReceiptIcon,
  SettingsIcon: SettingsIcon,
  ViewModuleIcon: ViewModuleIcon
};

export const getNavItems = yetkiler => {
  if (!yetkiler) {
    return [];
  }

  var menu = [
    {
      label: 'Ana Ekran',
      icon: 'DashboardTwoToneIcon',
      content: [
        {
          label: 'Satis',
          description: 'Satis Masasi',
          to: '/dashboardsatis'
        },
        {
          label: 'Teknik',
          description: 'Satis Masasi',
          to: '/dashboardteknik'
        }
      ]
    }
  ];

  if (yetkiler.musteri && yetkiler.musteri.includes('list')) {
    menu.push({
      label: 'Müşteriler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Aktif Müşteriller',
          description: '',
          to: '/musteriler?durum=aktif'
        },
        {
          label: 'Pasif Müşteriller',
          description: '',
          to: '/musteriler?durum=pasif'
        },
        {
          label: 'Potansiyel Müşteriller',
          description: '',
          to: '/musteriler?durum=potansiyel'
        },
        {
          label: 'Sorunlu Müşteriller',
          description: '',
          to: '/musteriler?durum=sorunlu'
        }
      ]
    });
  }
  if (yetkiler.urun && yetkiler.urun.includes('list')) {
    menu.push({
      label: 'Ürünler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Ürünler',
          description: '',
          to: '/urunler'
        }
      ]
    });
  }
  if (yetkiler.kisi && yetkiler.kisi.includes('list')) {
    menu.push({
      label: 'Kişiler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Kontak Kişiler',
          description: '',
          to: '/kisiler'
        }
      ]
    });
  }
  if (yetkiler.personel && yetkiler.personel.includes('list')) {
    menu.push({
      label: 'Personel',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Personel',
          description: '',
          to: '/personeller'
        }
      ]
    });
  }
  if (true) {
    menu.push({
      label: 'Görevler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Görevlerim',
          description: '',
          to: '/gorevler'
        }
      ]
    });
  }

  var navItems = [
    {
      label: 'İşlem Menusu',
      content: menu
    }
  ];
  return navItems;
};

export default [
  {
    label: 'Navigation menu',
    content: JSON.parse(
      `[
  {
    "label": "Ana Ekran",
    "icon": "DashboardTwoToneIcon",
    "content": [
      {
        "label": "Satış",
        "description": "Satış masası",
        "to": "/dashboardsatis"
      },
      {
        "label": "Teknik",
        "description": "This is a dashboard page example built using this template.",
        "to": "/DashboardDefault"
      },
      {
        "label": "Mali",
        "description": "This is a dashboard page example built using this template.",
        "to": "/DashboardDefault"
      }
    ]
  },
  {
    "label":"Müşteriler",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Aktif Müşteriller",
        "description": "",
        "to": "/musteriler?durum=aktif"
      },
      {
        "label": "Pasif Müşteriller",
        "description": "",
        "to": "/musteriler?durum=pasif"
      },
      {
        "label": "Potansiyel Müşteriller",
        "description": "",
        "to": "/musteriler?durum=potansiyel"
      },
      {
        "label": "Sorunlu Müşteriller",
        "description": "",
        "to": "/musteriler?durum=sorunlu"
      }
    ]
  },
  {
    "label":"Ürünler",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Ürünler",
        "description": "",
        "to": "/urunler"
      }
    ]
  },
  {
    "label":"Kontak Kişiler",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Kontak Kişiler",
        "description": "",
        "to": "/kisiler"
      }
    ]
  },
  {
    "label":"Mail",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Mail",
        "description": "",
        "to": "/toplantilar"
      }
    ]
  }
]`,
      (key, value) => {
        if (key === 'icon') {
          return iconsMap[value];
        } else {
          return value;
        }
      }
    )
  }
];
