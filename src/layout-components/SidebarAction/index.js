import React, { Fragment } from "react"
import { Button, Box} from "@material-ui/core"
import { withRouter } from "react-router"

const SidebarActions = props => {
    return (
        <Fragment>
            <Box>
                <div className="p-2">
                    <Button 
                        variant='contained' 
                        color="primary" 
                        fullWidth
                        onClick={() => props.history.push(`/gorevler`)}
                    >Görev Oluştur</Button>
                </div>
            </Box>
        </Fragment>
    )
}

export default withRouter(SidebarActions)