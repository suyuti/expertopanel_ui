import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  IconButton,
  Box,
  Typography,
  Menu,
  Button,
  List,
  ListItem
} from '@material-ui/core';

import avatar2 from '../../assets/images/avatars/avatar2.jpg';
import { connect } from 'react-redux';
import { compose } from 'redux';

import MoreVertIcon from '@material-ui/icons/MoreVert';

const SidebarUserbox = props => {
  const { sidebarToggle, sidebarHover } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);

  function openUserMenu(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  const {user, userPhoto} = props

  return (
    <Fragment>
      <Box
        className={clsx('app-sidebar-userbox', {
          'app-sidebar-userbox--collapsed': sidebarToggle && !sidebarHover
        })}>
        <Avatar
          alt={user && user.displayName}
          src={userPhoto}
          className="app-sidebar-userbox-avatar"
        />
        <Box className="app-sidebar-userbox-name">
          <Box>
            <b>{user && user.displayName}</b>
          </Box>
          <Box className="app-sidebar-userbox-description">
            {user && user.jobTitle}
          </Box>
          <Box className="app-sidebar-userbox-btn-profile">
            <Button
              size="small"
              color="secondary"
              variant="outlined"
              //className="text-white"
              onClick={() => {
                props.history.push(`/personeller/${user.id}`)}}
              >
              Profil Sayfam
            </Button>
          </Box>
        </Box>
      </Box>
    </Fragment>
  );
};

const mapStateToProps = state => ({
  sidebarToggle: state.ThemeOptions.sidebarToggle,
  sidebarHover: state.ThemeOptions.sidebarHover,
  user: state.ThemeOptions.me,
  userPhoto: state.ThemeOptions.mePhoto,
});

//export default connect(mapStateToProps)(SidebarUserbox);
export default compose(
  withRouter,
  connect(mapStateToProps)
)(SidebarUserbox);