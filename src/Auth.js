import React, { Component, Fragment, useEffect } from "react";
import { UserAgentApplication } from 'msal';
import { config } from './Config';
import {silentLogin} from './services/graph_Service'
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router";
import {withRouter} from 'react-router-dom'

const userAgentApplication = new UserAgentApplication({
    auth: {
      clientId: config.appId,
      redirectUri: config.redirectUri
    },
    cache: {
      cacheLocation: 'sessionStorage',
      storeAuthStateInCookie: true
    }
  });
  

class _Auth extends Component {
    state ={
        waitAuthCheck : true
    }
    componentDidMount() {
        //silentLogin().then(this.setState({waitAuthCheck: false}))
        
/*        return Promise.all([
            silentLogin()
        ]).then(() => {
            this.setState({waitAuthCheck:false})
        })*/
    }
    msCheck = async () => {
        new Promise(resolve => {
            debugger
            userAgentApplication.acquireTokenSilent({
                scopes: config.scopes
              }).then(res => 
                {
                    window.localStorage.setItem('token', res.accessToken)
                    resolve(res.accessToken)})
        })
    }
    render () {
        return this.state.waitAuthCheck ? <></> : <>{this.props.children}</>
    }
}

const Auth = props => {
    const me = useSelector(state => state.ThemeOptions.me)

    if (!me) {
        return <Redirect to='/' />
    }
    else {
        return (
            <Fragment>
                {props.children}
            </Fragment>
        )
    }
}

export default withRouter(Auth)